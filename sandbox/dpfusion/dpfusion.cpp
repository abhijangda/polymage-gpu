#define PY_SSIZE_T_CLEAN

#include <Python.h>
#include <algorithm>
#include <boost/multiprecision/cpp_int.hpp>
#include <cctype>
#include <fstream>
#include <functional>
#include <iostream>
#include <locale>
#include <map>
#include <math.h>
#include <numeric>
#include <queue>
#include <regex>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "Group.h"

using boost::multiprecision::uint128_t;
using namespace boost;
using namespace std;

/**
 * Hash function fo PyObject*
 **/
struct PyObjectHasher {
  uint64_t operator()(const PyObject *obj) const { return (uint64_t)obj; }
};

/**
 * Compare function for PyObject
 **/
struct PyObjectPred {
  bool operator()(PyObject *obj1, PyObject *obj2) const {
    return (bool)PyObject_RichCompareBool(obj1, obj2, Py_EQ);
  }
};

/*Map of Adjacent Vertices for a group. */
typedef std::unordered_map<Group *, std::set<Group *, Group::GroupComparer>,
                           Group::GroupHasher>
    InlinedAdjacentType;
typedef std::unordered_set<Group *, Group::GroupHasher> UnorderedGroupSet;
typedef std::set<Group *, Group::GroupComparer> OrderedGroupSet;
typedef std::unordered_set<uint128_t, uint128Hasher> Uint128UnorderedSet;

static bool const checkForAssertions = false;
static bool const checkPythonExceptions = true;
static bool printDPFusionTimeAndChoices = false;

// DPFusion Parameters
static bool INLINING_ENABLED = false;
static bool MULTI_LEVEL_TILING_ENABLED = false;

// System Parameters. All sizes are in Bytes.
static int IMAGE_ELEMENT_SIZE;
static uint64_t L2_CACHE_SIZE;
static uint64_t L1_CACHE_SIZE;
static int N_CORES;

// Weights
static float DIM_STD_DEV_WEIGHT;
static float LIVE_SIZE_TO_TILE_SIZE_WEIGHT;
static float CLEANUP_THREADS_WEIGHT;
static float RELATIVE_OVERLAP_WEIGHT;

static int64_t grp_size;
static uint64_t logMaxChildren = 3;
static uint64_t log_increment_factor = 1;
static PyObject *reduction_cls;
static PyObject *small_comps;
static PyObject *comp_size_map;
static PyObject *tstencil_cls;
static PyObject *pipeline;
static PyObject *pygroup_topological_order;
static PyObject *pygroup_dim_reuse;
static PyObject *pylive_size;
static PyObject *pydim_size;
static PyObject *storage_mapping_get_dim_size;
static PyObject *cls_Storage;
static PyObject *get_overlapping_size_func;
/*Table of costs for hash ids*/
static std::unordered_map<uint128_t, uint64_t, uint128Hasher> T;
/*Table of hash id to its optimal hash id found*/
static std::unordered_map<uint128_t, uint128_t, uint128Hasher>
    hashIDToOptHashID;
/*Table of PyObject to Group*/
static std::unordered_map<PyObject *, Group *> pyToGroup;
/*Set of small computations*/
static std::unordered_set<PyObject *, PyObjectHasher, PyObjectPred>
    small_comps_set;
/*Table of tile sizes (in form of vector) found for hashid*/
static std::unordered_map<uint128_t, std::vector<uint64_t>, uint128Hasher>
    optHashIDToTileSizes;
/*Table of ComputeObject (of Python) to Group (of Python)*/
static std::unordered_map<PyObject *, PyObject *> pyGroupForComp;
/*Table of ComputeObject to original Storage Class*/
static std::unordered_map<PyObject *, PyObject *> comp_to_orig_storage;
/*Table of storage class to key*/
static std::unordered_map<PyObject *, string> stg_class_to_key;
/*Table of ComputeObject to Function (of Python)*/
static std::unordered_map<PyObject *, PyObject *> comp_to_func;
/*Table of Function (of Python) to its type*/
static std::unordered_map<PyObject *, PyObject *> func_to_typ;
/*Table of Function to number of dimensions*/
static std::unordered_map<PyObject *, long> func_to_ndims;
/*Table of Computation Object to maximum offsets*/
static std::unordered_map<PyObject *, vector<long>> comp_to_max_offsets;
/**/
static std::unordered_map<PyObject *, vector<PyObject *>>
    helper_storage_to_dim_storage;

/** Declaration of Static Fields
 * Of Classes
 **/
/*Table of Group for a hash ids */
std::unordered_map<uint128_t, Group *, uint128Hasher> Group::hashIDToGroup;
/*Table of OptGroup for hash ids*/
std::unordered_map<uint128_t, OptGroup *, uint128Hasher>
    OptGroup::optHashIDToGroup;
/*Vector of all OptGroups found in current iterations*/
vector<OptGroup *> OptGroup::vectorOptGroups;
/**/
std::unordered_map<uint128_t, OptGroup *, uint128Hasher>
    OptGroup::parentOptGroupsToHashID;

bool OptGroupComparer::operator() (const OptGroup* g1, const OptGroup* g2) const
{
  return g1->hashID() < g2->hashID();
}

/**
 * Convert a Python String object to C++ STD String
 **/
string pystring_to_string(PyObject *str) {
  char *_str;
  string cxx_str;
  PyObject *pyStr;

  pyStr = PyUnicode_AsEncodedString(str, "utf-8", "Error ~");
  _str = PyBytes_AS_STRING(pyStr);
  cxx_str = string(_str);
  return cxx_str;
}

/**
 * Returns the number of ones in the hash_id, i.e. the number of children in
 * a group
 **/
inline uint64_t numberOfOnes(uint128_t hash_id) {
  uint64_t bit = 0;
  uint64_t nOnes = 0;

  while (hash_id != 0) {
    if ((hash_id & 1L) == 1) {
      nOnes++;
    }

    hash_id = hash_id >> 1;
    bit++;
  }

  return nOnes;
}

/**
 * Returns the position of first rightmost "1" in the hash_id
 **/
inline uint64_t first_one(uint128_t hash_id) {
  uint64_t bit = 0;

  while (hash_id != 0) {
    if ((hash_id & 1L) == 1) {
      return bit;
    }

    hash_id = hash_id >> 1;
    bit++;
  }

  return -1;
}

/**
 * Returns number of dimensions for Group (of Python)
 **/
uint64_t get_dim_size_for_pygroup(PyObject *group, int dim) {
  PyObject *dims_size;

  dims_size = PyDict_GetItem(pydim_size, group);

  return (uint64_t)PyLong_AsLong(PyList_GetItem(dims_size, dim));
}

/**
 * Return the dimensional resuse (list of reuse in each dimension) for a Group
 **/
int get_dim_reuse_for_pygroup(PyObject *group, int dim) {
  PyObject *dim_reuse;

  dim_reuse = PyDict_GetItem(pygroup_dim_reuse, group);

  return (int)PyLong_AsLong(PyList_GetItem(dim_reuse, dim));
}

/**
 * Check if there has been a Python exception and if true print and abort
 **/
void check_and_print_exception() {
  if (PyErr_Occurred() != NULL) {
    PyErr_Print();
    abort();
  }
}

/**
 * Returns the nubmber of dimensions for Group (of Python)
 **/
int get_n_dimensions(PyObject *group) {
  PyObject *dim_reuse;

  dim_reuse = PyDict_GetItem(pygroup_dim_reuse, group);

  return (int)PyList_Size(dim_reuse);
}

/**
 * Returns live size for Group (of Python)
 **/
uint64_t get_live_size(PyObject *group) {
  PyObject *size;

  size = PyDict_GetItem(pylive_size, group);

  return (uint64_t)PyLong_AsLong(size);
}

/**
 * Iterates over all children of a group and returns the maximum dimension.
 **/
int get_max_dimensions_for_group(uint128_t hash_id) {
  uint64_t bit = 0;
  int max_dim = 0;

  while (hash_id != 0) {
    if ((hash_id & 1L) == 1) {
      uint128_t l = 1;
      PyObject *pyGroup = Group::hashIDToGroup[l << bit]->getPyGroup();

      if (pyGroup != NULL) {
        int dim = get_n_dimensions(pyGroup);

        if (max_dim < dim)
          max_dim = dim;
      }
    }

    hash_id = hash_id >> 1;
    bit++;
  }

  return max_dim;
}

/**
 * Returns ComputeObject at an index for a Group.
 **/
PyObject *getCompForPyGroup(PyObject *pyGroup, int index) {
  static PyObject *str_comps = Py_BuildValue("s", "comps");
  PyObject *comps;
  comps = PyObject_GetAttr(pyGroup, str_comps);

  return PyList_GetItem(comps, index);
}

/**
 * Returns the Function name for given ComputeObject.
 **/
char *getPyCompFuncName(PyObject *comp) {
  static PyObject *str_func = Py_BuildValue("s", "func");
  static PyObject *str_name = Py_BuildValue("s", "name");

  PyObject *func;
  PyObject *name;
  PyObject *pyStr;

  func = PyObject_GetAttr(comp, str_func);
  name = PyObject_GetAttr(func, str_name);
  pyStr = PyUnicode_AsEncodedString(name, "utf-8", "Error ~");

  return PyBytes_AS_STRING(pyStr);
}

/**
 * Returns the name of Group (Python).
 **/
char *getPyGroupName(PyObject *pygroup) {
  if (pygroup == nullptr) {
    std::string s = "Dummy Group";
    char *ret = new char[s.length()];

    strcpy(ret, s.c_str());

    return ret;
  }

  PyObject *name;
  PyObject *pyStr;
  name = PyObject_Str(pygroup);
  pyStr = PyUnicode_AsEncodedString(name, "utf-8", "Error ~");

  return PyBytes_AS_STRING(pyStr);
}

/**
 * Returns the number of incoming edges to the children of group.
 **/
uint64_t getNumberOfInputs(uint128_t hash_id) {
  uint64_t bit;
  uint64_t nOnes;
  uint64_t input_size;
  uint128_t _hash_id;

  bit = 0;
  nOnes = 0;
  input_size = 0;
  _hash_id = hash_id;

  while (_hash_id != 0) {
    if ((_hash_id & 1L) == 1) {
      uint128_t l;
      Group *group;

      l = 1;
      group = Group::hashIDToGroup[l << bit];
      nOnes++;

      if (group->prevGroups().size() == 0) {
        input_size++;
      }

      /**
       * All previous groups that are not in current group are inputs
       **/
      for (auto it = group->prevGroups().begin();
           it != group->prevGroups().end(); it++) {
        if ((hash_id & (*it)->hashID()) != (*it)->hashID()) {
          input_size++;
        }
      }
    }

    _hash_id = _hash_id >> 1;
    bit++;
  }

  return input_size;
}

/**
 * Returns the level order traversal in *order* of children in a group.
 *
 * hash_id: HashID of Group
 * objs: ComputeObjects of Group (No need, remove as an argument)
 * vec_groups: Children of groups
 * order: level order traversal to be returned in.
 * inlined_groups: Set of groups inlined (No need, remove as an argument)
 * new_nextGroups: Map of Group to Next Groups
 * new_prevGroups: Map of Group to Prev Groups
 **/
void get_level_order(uint128_t hash_id, vector<PyObject *> &objs,
                     const vector<Group *> &vec_groups,
                     map<Group *, int, Group::GroupComparer> &order,
                     UnorderedGroupSet &inlined_groups,
                     InlinedAdjacentType &new_nextGroups,
                     InlinedAdjacentType &new_prevGroups) {
  static PyObject *str_func = Py_BuildValue("s", "func");

  bool change;

  change = true;

  for (auto it = vec_groups.begin(); it != vec_groups.end(); it++) {
    order[*it] = 0;
  }

  while (change) {
    change = false;

    for (auto obj = vec_groups.begin(); obj != vec_groups.end(); obj++) {
      for (auto &prev : new_prevGroups[*obj]) {
        if ((prev->hashID() & hash_id) == prev->hashID()) {
          if (order.find(prev) != order.end() && order[prev] >= order[*obj]) {
            order[*obj] = order[prev] + 1;
            change = true;
          }
        }
      }
    }
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "get_level_order(): order =" << std::endl;
    for (auto it = order.begin(); it != order.end(); it++) {
      PyObject *func;
      PyObject *name;
      PyObject *pyStr;
      const char *ss;

      func = PyObject_GetAttr(it->first->getCompAtIndex(0), str_func);
      name = PyObject_GetAttr(func, Py_BuildValue("s", "name"));
      pyStr = PyUnicode_AsEncodedString(name, "utf-8", "Error ~");
      ss = PyBytes_AS_STRING(pyStr);
      std::cout << "     " << ss << it->second << std::endl;
    }
  }
}

/**
 * C++ implementation of Python Function schedule.naive_sched_objs
 * order: schedule order of Groups
 * naive_order: naive_sched to be returned
 * new_nextGroups and new_prevGroups: Map of Next and Previous Groups
 *                                    for a group
 **/
void naive_sched_objs(const map<Group *, int, Group::GroupComparer> &order,
                      unordered_map<Group *, int> &naive_order,
                      const InlinedAdjacentType &new_nextGroups,
                      const InlinedAdjacentType &new_prevGroups) {
  static PyObject *str_func = Py_BuildValue("s", "func");
  unordered_map<int, vector<Group *>> reverse_map;
  int max_level;
  int time;

  time = 0;
  max_level = 0;

  for (auto it = order.begin(); it != order.end(); it++) {
    int l;

    l = it->second;
    if (reverse_map.find(l) == reverse_map.end()) {
      reverse_map[l] = std::vector<Group *>();
      reverse_map[l].push_back(it->first);
    } else {
      reverse_map[l].push_back(it->first);
    }

    max_level = max(max_level, l);
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "naive_sched_objs(): reverse_map = " << std::endl;
    for (auto it = reverse_map.begin(); it != reverse_map.end(); it++) {
      std::cout << it->first << std::endl;
      for (auto &it2 : it->second) {
        std::cout << "    " << getPyGroupName(it2->getPyGroup());
        std::cout << std::endl;
      }
    }
  }

  for (int l = 0; l <= max_level; l++) {
    for (auto obj = reverse_map[l].begin(); obj != reverse_map[l].end();
         obj++) {
      naive_order[*obj] = time;
      time += 1;
    }

    if (l != max_level) {
      std::vector<Group *> next_level;

      for (auto obj = reverse_map[l].begin(); obj != reverse_map[l].end();
           obj++) {
        vector<Group *> obj_kids;

        for (auto kid = reverse_map[l + 1].begin();
             kid != reverse_map[l + 1].end(); kid++) {
          if (new_nextGroups.at(*obj).find(*kid) !=
              new_nextGroups.at(*obj).end()) {
            obj_kids.push_back(*kid);
          }

          if (std::find(next_level.begin(), next_level.end(), *kid) ==
              next_level.end()) {
            next_level.push_back(*kid);
          }
        }
      }

      for (auto obj = reverse_map[l + 1].begin();
           obj != reverse_map[l + 1].end(); obj++) {
        if (std::find(next_level.begin(), next_level.end(), *obj) ==
            next_level.end()) {
          next_level.push_back(*obj);
        }
      }

      reverse_map[l + 1] = next_level;
    }
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "naive_sched_objs(): naive_order = " << std::endl;
    for (auto it = naive_order.begin(); it != naive_order.end(); it++) {
      PyObject *func;
      PyObject *name;
      PyObject *pyStr;
      const char *ss;

      func = PyObject_GetAttr(it->first->getCompAtIndex(0), str_func);
      name = PyObject_GetAttr(func, Py_BuildValue("s", "name"));
      pyStr = PyUnicode_AsEncodedString(name, "utf-8", "Error ~");
      ss = PyBytes_AS_STRING(pyStr);
      std::cout << "     " << ss << "  " << it->second << std::endl;
    }
  }
}

/**
 * C++ implementation of Python Function storage_mapping.classify_storage
 *
 * vec_comps: Vector of instances of class pipe.ComputeObject.
 * comp_to_stg_class: Map of pipe.ComputeObject to storage_mapping.Storage
 * new_storage_class_map: Map of objects of storage_mapping.Storage to vector
 *                        of pipe.ComputeObject
 **/
void classify_storage(
    const vector<PyObject *> &vec_comps,
    unordered_map<PyObject *, PyObject *> &comp_to_stg_class,
    unordered_map<PyObject *, vector<PyObject *>> &new_storage_class_map) {
  // find_storage_equivalence
  unordered_map<string, vector<PyObject *>> storage_class_map;

  for (auto it = vec_comps.begin(); it != vec_comps.end(); it++) {
    string _strkey;

    _strkey = stg_class_to_key[comp_to_orig_storage[*it]];
    if (storage_class_map.find(_strkey) == storage_class_map.end()) {
      storage_class_map[_strkey] = std::vector<PyObject *>();
      storage_class_map[_strkey].push_back(*it);
    } else {
      storage_class_map[_strkey].push_back(*it);
    }
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "classify_storage(): storage_class_map = " << std::endl;
    for (auto it = storage_class_map.begin(); it != storage_class_map.end();
         it++) {
      std::cout << "    " << it->first << " " << it->second.size();
      std::cout << std::endl;
    }
  }

  // maximal_storage
  for (auto it = storage_class_map.begin(); it != storage_class_map.end();
       it++) {
    string key;
    vector<PyObject *> &class_comps = it->second;
    PyObject *helper_comp;
    PyObject *func;
    PyObject *typ;
    long dims;
    PyObject *helper_storage;
    vector<long> max_offset;

    key = it->first;
    helper_comp = class_comps[0];
    func = comp_to_func[helper_comp];
    typ = func_to_typ[func];
    dims = func_to_ndims[func];
    helper_storage = comp_to_orig_storage[helper_comp];
    max_offset = comp_to_max_offsets[helper_comp];

    for (auto it2 = class_comps.begin(); it2 != class_comps.end(); it2++) {
      vector<long> &offset = comp_to_max_offsets[*it2];

      for (long i = 0; i < dims; i++) {
        long dim_off;

        dim_off = offset[i];
        max_offset[i] = max(max_offset[i], dim_off);
      }
    }

    vector<PyObject *> vec_dim_sizes; // vector of tuples

    for (long dim = 0; dim < dims; dim++) {
      static PyObject *str_orig_param = Py_BuildValue("s", "orig_param");
      PyObject *dim_storage;
      PyObject *new_size;
      PyObject *args;
      PyObject *max_offset_dim;

      max_offset_dim = PyLong_FromLong(max_offset[dim]);
      dim_storage = helper_storage_to_dim_storage[helper_storage][dim];
      args = PyTuple_Pack(2, dim_storage, max_offset_dim);
      new_size = PyObject_CallObject(storage_mapping_get_dim_size, args);
      Py_DECREF(args);
      Py_DECREF(max_offset_dim);

      vec_dim_sizes.push_back(PyTuple_Pack(
          2, PyObject_GetAttr(dim_storage, str_orig_param), new_size));
    }

    PyObject *dim_sizes;

    dim_sizes = PyList_New(vec_dim_sizes.size());

    for (auto it2 = vec_dim_sizes.begin(); it2 != vec_dim_sizes.end(); it2++) {
      PyList_SetItem(dim_sizes, (it2 - vec_dim_sizes.begin()), *it2);
    }

    PyObject *max_storage;
    PyObject *py_dims;
    PyObject *args;

    py_dims = PyLong_FromLong(dims);
    args = PyTuple_Pack(3, typ, py_dims, dim_sizes);
    max_storage = PyObject_CallObject(cls_Storage, args);
    Py_DECREF(args);
    Py_DECREF(py_dims);
    new_storage_class_map[max_storage] = vector<PyObject *>();

    for (auto it2 = class_comps.begin(); it2 != class_comps.end(); it2++) {
      comp_to_stg_class[*it2] = max_storage;
      new_storage_class_map[max_storage].push_back(*it2);
    }

    Py_DECREF(dim_sizes);

    for (auto it2 = vec_dim_sizes.begin(); it2 != vec_dim_sizes.end(); it2++) {
      PyObject *tuple = *it2;
      PyObject *tuple1 = PyTuple_GetItem(tuple, 1);
      Py_DECREF(tuple);
      Py_DECREF(tuple1);
    }
  }
}

/**
 * Get Liveness Map of all computations in a group, given a schedule.
 *
 * hash_id: hash_id of group.
 * vec_groups: vector of all computations in a group.
 * schedule: Map of class Group* to position of Group in the schedule.
 * liveness_map: Map of liveness position (in integer) to vector of
 * computations. To be returned. new_nextGroups: Map of a Group to its next
 * groups.
 */
void getLivenessMap(const uint128_t hash_id, const vector<Group *> &vec_groups,
                    const unordered_map<Group *, int> &schedule,
                    unordered_map<int, vector<PyObject *>> &liveness_map,
                    InlinedAdjacentType &new_nextGroups) {
  for (auto it = schedule.begin(); it != schedule.end(); it++) {
    int last_live;

    last_live = -1;

    for (auto &child : new_nextGroups[it->first]) {
      if ((child->hashID() & hash_id) == child->hashID()) {
        last_live = max(last_live, schedule.at(child));
      }
    }

    if (liveness_map.find(last_live) == liveness_map.end())
      liveness_map[last_live] = std::vector<PyObject *>();

    liveness_map[last_live].push_back(it->first->getCompAtIndex(0));
  }
}

/**
 * Returns sum of size of all liveouts for given group. Also sets n_liveouts to
 * the number of liveouts.
 * hashID: Hash ID of group
 * vec_groups: Vector of Group's Children
 * n_liveouts: Number of liveouts.
 **/

uint64_t getLiveoutsSize(uint128_t hash_id, vector<Group *> vec_groups,
                         int &n_liveouts) {
  uint64_t liveouts_size;

  liveouts_size = 0;
  n_liveouts = 0;

  for (auto const &group : vec_groups) {
    for (auto const &next_group : group->nextGroups()) {
      // If next_group is not in current group then it is a liveout
      if ((next_group->hashID() & hash_id) != next_group->hashID()) {
        PyObject *pygroup;
        uint64_t stg_size;

        pygroup = next_group->getPyGroup();
        stg_size = 1;
        for (int i = 0; i < get_n_dimensions(pygroup); i++) {
          // TODO: Optimize it because for every get_dim_reuse_for_pygroup call
          // dict and list are accessed again. Make them access only once
          // and then iterate over list
          stg_size *= get_dim_size_for_pygroup(pygroup, i);
        }

        liveouts_size += stg_size * IMAGE_ELEMENT_SIZE;
        n_liveouts++;
        break;
      }
    }

    if (group->nextGroups().size() == 0) {
      /**
       * Consider if the group is a liveout of the pipeline
       **/
      PyObject *pygroup;
      uint64_t stg_size;

      pygroup = group->getPyGroup();
      stg_size = 1;
      for (int i = 0; i < get_n_dimensions(pygroup); i++) {
        // TODO: Optimize it because for every get_dim_reuse_for_pygroup call
        // dict and list are accessed again. Make them access only once
        // and then iterate over list
        stg_size *= get_dim_size_for_pygroup(pygroup, i);
      }

      liveouts_size += stg_size * IMAGE_ELEMENT_SIZE;
      n_liveouts++;
    }
  }

  return liveouts_size;
}

/**
 * Returns Total Size used by the group i.e. sum of size of all intermediate
 * buffers + sum of size of all liveouts
 * hash_id: HashID of Group
 * number_of_buffers: set to the number of intermediate buffers
 * liveouts_size: set to the size of live outs.
 * livein_size: set to the size of all liveins
 * inlined_groups: Groups that has been inlined
 * new_nextGroups: New next Groups after inlining
 * new_prevGroups: New prev Groups after inlining
 **/
uint64_t getTotalSizeUsed(uint128_t hash_id, int &number_of_buffers,
                          uint64_t &liveouts_size, uint64_t &livein_size,
                          UnorderedGroupSet &inlined_groups,
                          InlinedAdjacentType &new_nextGroups,
                          InlinedAdjacentType &new_prevGroups) {
  static PyObject *str_func = Py_BuildValue("s", "func");
  static PyObject *str_comps = Py_BuildValue("s", "comps");

  // TODO: Support TStencil
  map<Group *, int, Group::GroupComparer> level_order;
  map<int, PyObject *> sorted_comps;
  // naive_order is schedule
  unordered_map<Group *, int> naive_order;
  unordered_map<PyObject *, PyObject *> cmp_to_stg_class;
  unordered_map<PyObject *, vector<PyObject *>> storage_class_map;
  // Number of arrays for each storage class
  unordered_map<PyObject *, int> n_stg_class_arrays;
  // Pool from computation to the vector of integers available
  unordered_map<PyObject *, vector<uint64_t>> array_pool;
  // storage_map to store first index of array for each computation
  unordered_map<PyObject *, uint64_t> storage_map;
  unordered_map<int, vector<PyObject *>> liveness_map;
  unordered_set<PyObject *> stg_class_removed;
  unordered_set<Group *, Group::GroupHasher> live_in_groups;
  uint128_t _hash_id;
  vector<PyObject *> vec_comps;
  vector<Group *> vec_groups;
  vector<Group *> live_out_groups;
  uint64_t total_size;
  uint64_t array_count;
  int bit;

  total_size = 0;
  _hash_id = hash_id;
  liveouts_size = 0;
  uint128_t nonLiveOutsHashID = 0;
  bit = 0;
  array_count = 0;

  while (_hash_id != 0) {
    if ((_hash_id & 1L) == 1) {
      uint128_t l = 1;
      Group *_g = Group::hashIDToGroup[l << bit];
      PyObject *pyGroup = _g->getPyGroup();

      if (!pyGroup) {
        _hash_id = _hash_id >> 1;
        bit++;
        continue;
      }

      // Include only those groups and comps which are not inlined
      if ((INLINING_ENABLED &&
           inlined_groups.find(_g) == inlined_groups.end()) ||
          !INLINING_ENABLED) {
        if (!OptGroup::isLiveoutForGroup(hash_id, (OptGroup *)_g)) {
          PyObject *comps;

          comps = PyObject_GetAttr(pyGroup, str_comps);
          nonLiveOutsHashID |= (l << bit);
          vec_groups.push_back(_g);

          for (int j = 0; j < PyList_Size(comps); j++) {
            PyObject *comp = PyList_GetItem(comps, j);

            vec_comps.push_back(comp);
          }
        } else
          live_out_groups.push_back(_g);

        // TODO: include only those liveins which are not inlined
        OptGroup::liveinsForChildInGroup(hash_id, (OptGroup *)_g,
                                         live_in_groups);
      }
    }

    _hash_id = _hash_id >> 1;
    bit++;
  }

  get_level_order(nonLiveOutsHashID, vec_comps, vec_groups, level_order,
                  inlined_groups, new_nextGroups, new_prevGroups);
  naive_sched_objs(level_order, naive_order, new_nextGroups, new_prevGroups);
  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "getTotalSizeUsed(): naive_order = " << std::endl;
    for (auto it = naive_order.begin(); it != naive_order.end(); it++) {
      char *name = getPyCompFuncName(it->first->getCompAtIndex(0));
      std::cout << it->second << " " << name << std::endl;
    }
  }

  getLivenessMap(nonLiveOutsHashID, vec_groups, naive_order, liveness_map,
                 new_nextGroups);
  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "getTotalSizeUsed(): naive_order = " << std::endl;
    for (auto it = naive_order.begin(); it != naive_order.end(); it++) {
      char *name = getPyCompFuncName(it->first->getCompAtIndex(0));
      std::cout << it->second << " " << name << std::endl;
    }
  }

  classify_storage(vec_comps, cmp_to_stg_class, storage_class_map);
  liveouts_size = getLiveoutsSize(hash_id, live_out_groups, number_of_buffers);

  /*Get LiveIn Size*/
  for (auto const &__g : live_in_groups) {
    {
      PyObject *pygroup;
      uint64_t stg_size;

      pygroup = __g->getPyGroup();
      stg_size = 1;

      if (!pygroup)
        continue;

      for (int i = 0; i < get_n_dimensions(pygroup); i++) {
        // TODO: Optimize it because for every get_dim_reuse_for_pygroup call
        // dict and list are accessed again. Make them access only once
        // and then iterate over list
        stg_size *= get_dim_size_for_pygroup(pygroup, i);
      }

      livein_size += stg_size * IMAGE_ELEMENT_SIZE;
      break;
    }
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "getTotalSizeUsed(): number_of_buffers ";
    std::cout << number_of_buffers << std::endl;

    std::cout << "getTotalSizeUsed(): cmp_to_stg_class = " << std::endl;
    for (auto it = vec_comps.begin(); it != vec_comps.end(); it++) {
      PyObject *func;
      PyObject *name;
      PyObject *pyStr;
      const char *ss;

      func = PyObject_GetAttr(*it, str_func);
      name = PyObject_GetAttr(func, Py_BuildValue("s", "name"));
      pyStr = PyUnicode_AsEncodedString(name, "utf-8", "Error ~");
      ss = PyBytes_AS_STRING(pyStr);
      std::cout << "     " << ss << (cmp_to_stg_class[*it]) << std::endl;
    }
  }

  for (auto it = vec_comps.begin(); it != vec_comps.end(); it++) {
    array_pool[cmp_to_stg_class[*it]] = vector<uint64_t>();
  }

  for (auto it = naive_order.begin(); it != naive_order.end(); it++) {
    sorted_comps[it->second] = it->first->getCompAtIndex(0);
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "getTotalSizeUsed(): sorted comps = " << std::endl;
    for (auto it = sorted_comps.begin(); it != sorted_comps.end(); it++) {
      char *name = getPyCompFuncName(it->second);
      std::cout << it->first << " " << name << std::endl;
    }
  }

  for (auto it = sorted_comps.begin(); it != sorted_comps.end(); it++) {
    PyObject *comp;
    PyObject *storage_class;
    PyObject *func;
    uint64_t num_reqd;
    uint64_t num_available;
    uint64_t num_allocated;
    uint64_t deficit;
    bool is_tstencil_grp;
    vector<uint64_t> allocated_arrays;
    int time;

    comp = it->second;
    storage_class = cmp_to_stg_class[comp];
    func = comp_to_func[comp];
    is_tstencil_grp = false;

    if (PyObject_IsInstance(func, tstencil_cls)) {
      is_tstencil_grp = true;
    }

    if (is_tstencil_grp == true)
      num_reqd = 2;
    else
      num_reqd = 1;

    num_available = array_pool[storage_class].size();
    num_allocated = min(num_available, num_reqd);

    for (uint64_t i = 0; i < num_allocated; i++) {
      allocated_arrays.push_back(array_pool[storage_class].back());
      array_pool[storage_class].pop_back();
    }

    deficit = num_reqd - num_allocated;

    if (deficit > 0) {
      for (uint64_t i = 0; i < deficit; i++) {
        allocated_arrays.push_back(array_count + 1 + i);
      }

      array_count += deficit;
      n_stg_class_arrays[storage_class] += deficit;
    }

    storage_map[comp] = allocated_arrays[0];
    time = it->first;

    if (liveness_map.find(time) != liveness_map.end()) {
      vector<PyObject *> free_comps;
      PyObject *cmp_stg_class;

      free_comps = liveness_map[time];

      for (auto it2 = free_comps.begin(); it2 != free_comps.end(); it2++) {
        cmp_stg_class = cmp_to_stg_class[*it2];
        array_pool[cmp_stg_class].push_back(storage_map[*it2]);
      }
    }
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "getTotalSizeUsed(): array_pool = " << std::endl;
    for (auto it = array_pool.begin(); it != array_pool.end(); it++) {
      PyObject *name;
      PyObject *pyStr;
      const char *ss;

      name = PyObject_Str(it->first);
      pyStr = PyUnicode_AsEncodedString(name, "utf-8", "Error ~");
      ss = PyBytes_AS_STRING(pyStr);
      std::cout << ss << std::endl;

      for (auto it2 = it->second.begin(); it2 != it->second.end(); it2++) {
        std::cout << "   " << *it2 << std::endl;
      }
    }
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "getTotalSizeUsed(): array_count " << array_count;
    std::cout << "for hash_id " << std::bitset<41>((uint64_t)hash_id);
    std::cout << std::endl;
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "getTotalSizeUsed(): total_size ";
    std::cout << n_stg_class_arrays.size() << std::endl;
  }

  // Get size of intermediate buffers
  for (auto it = n_stg_class_arrays.begin(); it != n_stg_class_arrays.end();
       it++) {
    PyObject *stg_class;
    int n_arrays;
    PyObject *pygroup;
    uint64_t stg_size;

    stg_class = it->first;
    n_arrays = it->second;
    pygroup = pyGroupForComp[storage_class_map[stg_class][0]];
    stg_size = 1;
    for (int i = 0; i < get_n_dimensions(pygroup); i++) {
      // TODO: Optimize it because for every get_dim_reuse_for_pygroup call
      // dict and list are accessed again. Make them access only once
      // and then iterate over list
      uint64_t __size;

      __size = get_dim_size_for_pygroup(pygroup, i);
      stg_size *= __size;
    }

    total_size += stg_size * n_arrays * IMAGE_ELEMENT_SIZE;
  }

  for (auto it = storage_class_map.begin(); it != storage_class_map.end();
       it++) {
    Py_DECREF(it->first);
  }

  number_of_buffers += array_count;

  return total_size + liveouts_size;
}

/**
 * Returns the standard deviation of dim sizes of all children of group.
 * dim_size_diff: vector of dimension sizes of each child
 * max_dim: maximum number of dims of all children of a group.
 **/
int64_t dim_size_std_dev(std::vector<std::vector<uint64_t>> &dim_size_diff,
                         int max_dim) {
  // vectors for sum and mean in each dimension
  std::vector<double> sum;
  std::vector<double> mean;
  std::vector<double> mean_diff;
  double sum_mean_dim;
  double sum_mean_diff;

  mean_diff = std::vector<double>(max_dim, 0);
  sum = std::vector<double>(max_dim, 0);
  mean = std::vector<double>(max_dim, 0);

  for (auto it_comp = dim_size_diff.begin(); it_comp != dim_size_diff.end();
       it_comp++) {
    for (uint64_t dim = 0; dim < (*it_comp).size(); dim++) {
      sum[dim] += (*it_comp)[dim];
    }
  }

  for (uint64_t i = 0; i < sum.size(); i++) {
    // dim_size_diff.size () -> number of computations
    // sum [i]->sum of dim sizes in each dimension
    mean[i] = sum[i] / dim_size_diff.size();
  }

  // Calculate mean difference in each dimension

  for (auto it_comp = dim_size_diff.begin(); it_comp != dim_size_diff.end();
       it_comp++) {
    auto _size = dim_size_diff.size();

    for (uint64_t dim = 0; dim < (*it_comp).size(); dim++) {
      mean_diff[dim] += abs((*it_comp)[dim] - mean[dim]) / _size;
    }
  }

  sum_mean_dim = std::accumulate(mean.begin(), mean.end(), 0.0);
  sum_mean_diff = std::accumulate(mean_diff.begin(), mean_diff.end(), 0.0);
  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "mean sum" << std::accumulate(mean.begin(), mean.end(), 0.0);
    std::cout << " mean_diff sum "
              << std::accumulate(mean_diff.begin(), mean_diff.end(), 0.0)
              << std::endl;
  }

  if (sum_mean_diff / sum_mean_dim > 0.1f)
    return sum_mean_diff;
  else
    return 0;
}

/**
 * Return number of edges in a group.
 *
 **/
int getNumberOfEdges(uint128_t _hash_id) {
  vector<Group *> vec_groups;
  int n_edges;
  int bit;

  n_edges = 0;
  bit = 0;
  while (_hash_id != 0) {
    if ((_hash_id & 1L) == 1) {
      uint128_t l;
      Group *_g;
      PyObject *pyGroup;

      l = 1;
      _g = Group::hashIDToGroup[l << bit];
      pyGroup = _g->getPyGroup();

      if (!pyGroup) {
        _hash_id = _hash_id >> 1;
        bit++;
        continue;
      }

      for (auto const &next_group : _g->nextGroups()) {
        if ((next_group->hashID() & _hash_id) == next_group->hashID())
          n_edges++;
      }
    }

    _hash_id = _hash_id >> 1;
    bit++;
  }

  return n_edges;
}

/**
 * Update nextGroups and prevGroups maps for given inlined groups.
 **/
void update_inlining_next_prev_vertices(InlinedAdjacentType &new_nextGroups,
                                        InlinedAdjacentType &new_prevGroups,
                                        Group *inlined_group) {
  for (auto const &next : new_nextGroups[inlined_group]) {
    auto &next_prev = new_prevGroups[next];

    next_prev.erase(inlined_group);

    for (auto const &prev : new_prevGroups[inlined_group]) {
      next_prev.insert(prev);
    }
  }

  for (auto const &prev : new_prevGroups[inlined_group]) {
    auto &prev_next = new_nextGroups[prev];

    prev_next.erase(inlined_group);

    for (auto const &next : new_nextGroups[inlined_group]) {
      prev_next.insert(next);
    }
  }

  new_nextGroups.erase(inlined_group);
  new_prevGroups.erase(inlined_group);
}

/**
 * Finds the computations to be inlined for a given group.
 *
 * inlined_groups: Set of class Group to be inlined.
 * new_nextGroups and new_prevGroups: Updated next and prev groups.
 * hash_id: Original Group without inlining any computations.
 **/
void update_graph_with_inlining(UnorderedGroupSet &inlined_groups,
                                InlinedAdjacentType &new_nextGroups,
                                InlinedAdjacentType &new_prevGroups,
                                uint128_t hash_id) {
  static PyObject *str_is_pointwise = Py_BuildValue("s", "is_pointwise");
  static PyObject *str_inline_all_children =
      Py_BuildValue("s", "inline_in_all_children");
  std::vector<Group *> groups;
  std::unordered_set<Group *> pointwise_groups;
  std::unordered_set<Group *> inline_all_children_group;
  UnorderedGroupSet liveouts;
  UnorderedGroupSet new_inlined_groups;
  std::queue<Group *> bfs_queue;
  uint128_t _hash_id;
  int bit;

  _hash_id = hash_id;
  bit = 0;

  while (_hash_id != 0) {
    if ((_hash_id & 1L) == 1) {
      uint128_t l;
      Group *group;

      l = 1L;
      group = Group::hashIDToGroup[l << bit];
      if (group->getPyGroup() != nullptr) {
        groups.push_back(group);
        PyObject *pointwise_func;
        PyObject *is_pointwise;

        pointwise_func =
            PyObject_GetAttr(group->getPyGroup(), str_is_pointwise);
        is_pointwise = PyObject_CallObject(pointwise_func, NULL);
        // TODO: Get all pointwise functions as argument to dpgroup function
        // to increase the speed a little bit
        if (is_pointwise == Py_True) {
          pointwise_groups.insert(group);
        } else {
          PyObject *inline_all_func;
          inline_all_func =
              PyObject_GetAttr(group->getPyGroup(), str_inline_all_children);
          PyObject *inline_all;
          inline_all = PyObject_CallObject(inline_all_func, NULL);

          if (inline_all == Py_True) {
            inline_all_children_group.insert(group);
          }
        }
      }
    }
    bit++;
    _hash_id = _hash_id >> 1;
  }

  // Dummy Group
  if (groups.size() == 0) {
    return;
  }

  if (groups.size() == 1) {
    // Single Group, don't need to do anything
    return;
  }

  if (pointwise_groups.size() == 0) {
    // No Pointwise groups means no inlining
    return;
  }

  // Start from the liveouts. Traverse from liveouts to the liveins in a BFS
  // fashion not a DFS and inline all the pointwise functions found, until
  // there is no spilling, current producer is not a liveout, and the
  // current producer has only one consumer because inlining a producer twice
  // or more will lead to redundant computations.
  // Moreover, inline the producers of pointwise functions if they
  // are not liveouts and don't have two consumers, until there is
  // no spilling. Also, inlining of producer into consumer is only done
  // when the sizes of each dimensions are same.
  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "update_graph_with_inlining(): Number of Pointwise groups ";
    std::cout << pointwise_groups.size() << std::endl;
    std::cout << "update_graph_with_inlining(): ";
    std::cout << "Pointwise groups are: " << std::endl;
    for (auto const &it : pointwise_groups) {
      std::cout << getPyGroupName(it->getPyGroup()) << ", ";
    }

    std::cout << std::endl;

    std::cout << "update_graph_with_inlining(): ";
    std::cout << "Number of inline_all_children_group groups  ";
    std::cout << inline_all_children_group.size() << std::endl;
    std::cout << "update_graph_with_inlining(): ";
    std::cout << "inline_all_children_group groups are: " << std::endl;
    for (auto const &it : inline_all_children_group) {
      std::cout << getPyGroupName(it->getPyGroup()) << ", ";
    }

    std::cout << std::endl;
  }

  OptGroup::liveoutsForGroup(hash_id, groups, liveouts);
  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "update_graph_with_inlining(): liveouts for group ";
    std::cout << liveouts.size() << std::endl;
  }

  // Do BFS and find all pointwise_groups in reverse
  for (auto const &liveout : liveouts) {
    bfs_queue.push(liveout);

    while (bfs_queue.size() != 0) {
      Group *_front = bfs_queue.front();
      bfs_queue.pop();
      // Inline those functions which:
      // (i) are pointwise, not liveouts, and have only one consumer.
      // (ii) are pointwise, have more than 1 consumer, but contains only
      //      one reference
      bool _to_inline = false;
      if (pointwise_groups.count(_front) > 0 && liveouts.count(_front) == 0 &&
          _front->nextGroups().size() == 1 &&
          (_front->hashID() & hash_id) == _front->hashID()) {
        _to_inline = true;
      }

      if (!_to_inline && pointwise_groups.count(_front) > 0 &&
          liveouts.count(_front) == 0 && _front->nextGroups().size() > 0 &&
          (_front->hashID() & hash_id) == _front->hashID() &&
          inline_all_children_group.count(_front) > 0) {
        _to_inline = true;
      }

      if (_to_inline) {
        inlined_groups.insert(_front);
        update_inlining_next_prev_vertices(new_nextGroups, new_prevGroups,
                                           _front);
      }

      for (auto const &prev : _front->prevGroups()) {
        // Prev Group should be in this group
        if ((hash_id & prev->hashID()) == prev->hashID())
          bfs_queue.push(prev);
      }
    }
  }
  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "update_graph_with_inlining():";
    std::cout << "Inlined Pointwise and non liveout Groups found: ";
    std::cout << inlined_groups.size() << std::endl;
    for (auto const &g : inlined_groups) {
      std::cout << getPyGroupName(g->getPyGroup()) << ", ";
    }

    std::cout << std::endl;
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "update_graph_with_inlining():";
    std::cout << "Updated Next Groups after inlining = " << std::endl;
    for (auto const &it : new_nextGroups) {
      std::cout << getPyGroupName(it.first->getPyGroup()) << " -> ";

      for (auto const &g : it.second) {
        std::cout << getPyGroupName(g->getPyGroup()) << ", ";
      }

      std::cout << std::endl;
    }

    std::cout << "update_graph_with_inlining():";
    std::cout << "Updated Prev Groups after inlining = " << std::endl;
    for (auto const &it : new_prevGroups) {
      std::cout << getPyGroupName(it.first->getPyGroup()) << " <- ";

      for (auto const &g : it.second) {
        std::cout << getPyGroupName(g->getPyGroup()) << ", ";
      }

      std::cout << std::endl;
    }
  }

  // At this point we have inlined all the pointwise functions (and
  // non-liveouts, i.e., intermediate functions) into their respective children.
  // In the last stage, again find all the pointwise functions (but also taking
  // into account liveouts) and then inline the producer of these pointwise
  // functions only if they have only one consumer.

  // TODO: an optimization here can be to consider only pointwise liveouts
  // and not the whole DAG, since, all pointwise functions before has
  // been inlined.
  // Do BFS and find all pointwise_groups in reverse using updated
  // New and Prev Groups Set
  for (auto const &liveout : liveouts) {
    bfs_queue.push(liveout);

    while (bfs_queue.size() != 0) {
      Group *_front;

      _front = bfs_queue.front();
      bfs_queue.pop();
      // Inline the producer functions which
      // (i) are producer functions for pointwise and has only one consumer
      // (ii) are not pointwise and contains several pointwise children but
      //     contains Case statement in body for each children, like
      //     deinterleaved in Camera Pipe (in other words, consumer should
      //     be in inline_all_children_group)
      if (pointwise_groups.find(_front) != pointwise_groups.end()) {
        for (auto const &it : new_prevGroups[_front]) {
          bool inline_producer;

          inline_producer = false;

          if (new_nextGroups[it].size() == 1 &&
              (it->hashID() & hash_id) == it->hashID() &&
              liveouts.count(it) == 0) {
            inline_producer = true;
          }

          if (new_nextGroups[it].size() > 1 &&
              inline_all_children_group.count(it) == 1 &&
              (it->hashID() & hash_id) == it->hashID() &&
              liveouts.count(it) == 0) {
            inline_producer = true;
          }

          if (inline_producer) {
            inlined_groups.insert(it);
            new_inlined_groups.insert(it);
          }
        }
      }

      for (auto const &prev : new_prevGroups[_front]) {
        // Prev Group should be in this group
        if ((hash_id & prev->hashID()) == prev->hashID())
          bfs_queue.push(prev);
      }
    }
  }

  // Update New Next and Prev Groups set

  for (auto const &it : new_inlined_groups) {
    update_inlining_next_prev_vertices(new_nextGroups, new_prevGroups, it);
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "update_graph_with_inlining(): Inlined Groups = ";
    std::cout << inlined_groups.size() << std::endl;
    for (auto const &g : inlined_groups) {
      std::cout << getPyGroupName(g->getPyGroup()) << ", ";
    }

    std::cout << std::endl;
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "update_graph_with_inlining():";
    std::cout << "Updated Next Groups after inlining =  " << std::endl;
    for (auto const &it : new_nextGroups) {
      std::cout << getPyGroupName(it.first->getPyGroup()) << " -> ";

      for (auto const &g : it.second) {
        std::cout << getPyGroupName(g->getPyGroup()) << ", ";
      }

      std::cout << std::endl;
    }

    std::cout << "update_graph_with_inlining():";
    std::cout << "Updated Prev Groups after inlining =  " << std::endl;
    for (auto const &it : new_prevGroups) {
      std::cout << getPyGroupName(it.first->getPyGroup()) << " <- ";

      for (auto const &g : it.second) {
        std::cout << getPyGroupName(g->getPyGroup()) << ", ";
      }

      std::cout << std::endl;
    }
  }
}

/**
 * Returns the cost of a group given by hash_id and the tile_sizes.
 *
 **/
inline uint64_t cost(uint128_t hash_id, std::vector<uint64_t> &tile_sizes) {
  static PyObject *str_comps = Py_BuildValue("s", "comps");
  static PyObject *str_func = Py_BuildValue("s", "func");
  static PyObject *str_is_const_func = Py_BuildValue("s", "is_const_func");

  uint64_t bit;
  uint64_t nOnes;
  uint64_t liveouts_size;
  uint64_t liveins_size;
  uint64_t tile_size;
  uint64_t totalsizeused;
  int64_t mean_dim_diff;
  int64_t overlap_size;
  bool is_reduction;
  bool is_small_grp;
  bool is_tstencil_grp;
  bool is_const_grp;
  bool is_dummy_source_group;
  bool all_n_dims_equal;
  int total_comps;
  int max_dim;
  int max_dims;
  int n_threads;
  int _cost;
  int n_buffers;
  int last_group_dim;
  float overlap_cost;
  float threads_ratio;
  float live_ratio;
  float dim_cost;
  // 2-D vector with rows as number of comps and cols as max_dim
  std::vector<std::vector<uint64_t>> dim_size_diff;
  UnorderedGroupSet inlined_groups;
  std::vector<uint64_t> dim_reuse;
  std::vector<uint64_t> dim_size;
  uint128_t _hash_id;
  PyObject *list_groups_for_overlap;
  PyObject *pytotalsizeused;
  PyObject *pyn_buffers;
  PyObject *list_inline_comps;
  PyObject *args;
  PyObject *tuple_return;
  PyObject *overlap_obj;
  PyObject *pytile_size;
  InlinedAdjacentType new_nextGroups;
  InlinedAdjacentType new_prevGroups;

  bit = 0;
  nOnes = 0;
  total_comps = 0;
  n_threads = 0;
  n_buffers = 0;
  last_group_dim = -1;
  liveouts_size = 0;
  liveins_size = 0;
  tile_size = 0;
  totalsizeused = 0;
  is_tstencil_grp = false;
  is_reduction = false;
  is_small_grp = false;
  is_const_grp = false;
  is_dummy_source_group = false;
  all_n_dims_equal = true;
  _hash_id = hash_id;
  list_groups_for_overlap = PyList_New(0);
  max_dim = get_max_dimensions_for_group(hash_id);
  max_dims = max_dim;
  dim_reuse = std::vector<uint64_t>(max_dims, 0);
  dim_size = std::vector<uint64_t>(max_dims, 0);

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "cost(): Cost for hash_id " << std::endl;
    std::cout << std::bitset<41>((uint64_t)hash_id) << std::endl;
  }

  // Collect information from Python about each python class Group instance.
  while (_hash_id != 0) {
    if ((_hash_id & 1L) == 1) {
      uint128_t l;
      Group *group;
      PyObject *pyGroup;

      l = 1;
      group = Group::hashIDToGroup[l << bit];
      pyGroup = group->getPyGroup();
      nOnes++;

      if (pyGroup != NULL) {
        PyObject *comps;

        comps = PyObject_GetAttr(pyGroup, str_comps);
        total_comps += PyList_Size(comps);

        for (int j = 0; j < PyList_Size(comps); j++) {
          PyObject *comp;
          PyObject *func;
          PyObject *is_const_func;

          comp = PyList_GetItem(comps, j);
          func = PyObject_GetAttr(comp, str_func);
          if (PyObject_IsInstance(func, reduction_cls)) {
            is_reduction = true;
          }

          if (PyObject_IsInstance(func, tstencil_cls)) {
            is_tstencil_grp = true;
          }

          is_const_func = PyObject_GetAttr(func, str_is_const_func);
          if (is_const_func == Py_True) {
            is_const_grp = true;
          }

          if (small_comps_set.find(comp) != small_comps_set.end()) {
            is_small_grp = true;
          }
        }

        dim_size_diff.push_back(std::vector<uint64_t>(max_dim, 0));

        for (int i = 0; i < get_n_dimensions(pyGroup); i++) {
          // TODO: Optimize it because for every
          // get_dim_reuse_for_pygroup call
          // dict and list are accessed again. Make them access only once
          // and then iterate over list
          // PyObject* comp = PyList_GetItem (comps, 0);
          // PyObject* func = PyObject_GetAttr (comp, str_func);
          uint64_t _dim_size;
          dim_reuse[i] += get_dim_reuse_for_pygroup(pyGroup, i);
          dim_reuse[i] *= IMAGE_ELEMENT_SIZE;
          _dim_size = get_dim_size_for_pygroup(pyGroup, i);
          _dim_size *= IMAGE_ELEMENT_SIZE;
          dim_size[i] += _dim_size;
          dim_size_diff[dim_size_diff.size() - 1][i] = _dim_size;
        }

        if (all_n_dims_equal == true) {
          if (last_group_dim != -1 and
              last_group_dim != get_n_dimensions(pyGroup)) {
            all_n_dims_equal = false;
          }

          last_group_dim = get_n_dimensions(pyGroup);
        }

        PyList_Append(list_groups_for_overlap, pyGroup);

        new_nextGroups[group] = OrderedGroupSet(group->nextGroups());
        new_prevGroups[group] = OrderedGroupSet(group->prevGroups());
      } else {
        is_dummy_source_group = true;
      }
    }

    _hash_id = _hash_id >> 1;
    bit++;
  }

  if (!printDPFusionTimeAndChoices) {
    if (INLINING_ENABLED)
      update_graph_with_inlining(inlined_groups, new_nextGroups, new_prevGroups,
                                 hash_id);

    for (auto &it : new_nextGroups) {
      Group *g;

      g = it.first;

      for (auto &next : it.second) {
        int next_n_dims = get_n_dimensions(next->getPyGroup());
        int g_dims = get_n_dimensions(g->getPyGroup());

        if ((hash_id & g->hashID()) == g->hashID() &&
            (hash_id & next->hashID()) == next->hashID() &&
            g_dims < next_n_dims) {
          return -1;
        }
      }
    }

    totalsizeused =
        getTotalSizeUsed(hash_id, n_buffers, liveouts_size, liveins_size,
                         inlined_groups, new_nextGroups, new_prevGroups);
  }

  mean_dim_diff = dim_size_std_dev(dim_size_diff, max_dims);

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "cost(): n_buffers     = " << n_buffers << std::endl;
    std::cout << "cost(): totalsizeused = " << totalsizeused << std::endl;
  }

  if (!printDPFusionTimeAndChoices) {
    if (totalsizeused == 0 && n_buffers == 0) {
      // Dummy group
      return 0;
    }

    _hash_id = hash_id;

    if ((is_reduction || is_const_grp || is_small_grp || is_tstencil_grp ||
         is_dummy_source_group) &&
        nOnes == 1)
      return 1L;

    if (is_reduction || is_const_grp || is_small_grp || is_tstencil_grp ||
        is_dummy_source_group)
      return -1;

    if (total_comps > grp_size)
      return -1;

    if (is_dummy_source_group)
      return -1;

    if (nOnes == 1)
      return INT_MAX;
  }

  if (printDPFusionTimeAndChoices) {
    if ((is_reduction || is_const_grp || is_small_grp || is_tstencil_grp) &&
        nOnes == 1)
      return 1L;

    if (is_reduction)
      return -1;

    return 1L;
  }

  // Call get_overlapping_size function of Python
  list_inline_comps = PyList_New(0);
  pytotalsizeused = PyLong_FromLong(totalsizeused);
  pyn_buffers = PyLong_FromLong(n_buffers);

  for (auto const &g : inlined_groups) {
    PyList_Append(list_inline_comps, g->getCompAtIndex(0));
  }

  args = PyTuple_Pack(4, list_groups_for_overlap, list_inline_comps,
                      pytotalsizeused, pyn_buffers);
  tuple_return = PyObject_CallObject(get_overlapping_size_func, args);
  overlap_obj = PyTuple_GetItem(tuple_return, 0);
  pytile_size = PyTuple_GetItem(tuple_return, 1);
  overlap_size = PyLong_AsLong(overlap_obj) * IMAGE_ELEMENT_SIZE;
  tile_size = PyLong_AsLong(pytile_size) * IMAGE_ELEMENT_SIZE;
  if (tile_size == 0 && overlap_size == -4) {
    return -1;
  }

  if (tile_size == 0 && overlap_size == 0) {
    tile_size = totalsizeused / N_CORES;
    if (tile_size > L2_CACHE_SIZE)
      tile_size = L2_CACHE_SIZE;

    overlap_size = 0;
  }

  if (tile_size == 4 && overlap_size == (1L << 30) * IMAGE_ELEMENT_SIZE) {
    return -1;
  }

  assert(tile_size != 0);
  Py_DECREF(args);
  Py_DECREF(list_inline_comps);
  Py_DECREF(pytotalsizeused);
  Py_DECREF(list_groups_for_overlap);
  Py_DECREF(pyn_buffers);

  n_threads = totalsizeused / (tile_size * n_buffers);

  overlap_cost =
      (RELATIVE_OVERLAP_WEIGHT * overlap_size) / (tile_size * n_buffers);
  threads_ratio =
      (((n_threads + N_CORES - 1) % N_CORES) * CLEANUP_THREADS_WEIGHT) /
      N_CORES;
  live_ratio =
      (LIVE_SIZE_TO_TILE_SIZE_WEIGHT * (liveins_size + liveouts_size)) /
      (tile_size * n_buffers);
  dim_cost = mean_dim_diff * DIM_STD_DEV_WEIGHT;

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "cost(): ";
    std::cout << "mean_dim_diff = " << mean_dim_diff << " ";
    std::cout << "liveins_size  = " << liveins_size << " ";
    std::cout << "liveouts_size = " << liveouts_size << " ";
    std::cout << "live ratio    = " << live_ratio << " ";
    std::cout << "threads ratio = " << threads_ratio << " ";
    std::cout << "all_n_dims_equal = " << all_n_dims_equal << " ";
    std::cout << "overlap = " << overlap_size << " ";
    std::cout << "tile_size = " << tile_size << " ";
    std::cout << "overlap cost = " << overlap_cost << " ";
    std::cout << "n_buffers = " << n_buffers << std::endl;
  }

  _cost = dim_cost + live_ratio - threads_ratio + overlap_cost;

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "cost(): final cost = " << _cost << std::endl;
  }

  if (total_comps <= grp_size) {
    return _cost;
  }

  return -1;
}

/**
 * Returns true if start is reachable to end.
 * If isEndGroup is enabled then return true if any computation reachable from
 * start is a part of end, otherwise return true only if end is reachable.
 *
 **/
inline bool reachable(uint128_t start, uint128_t end, bool isEndGroup) {
  queue<uint128_t> q;

  q.push(start);

  while (!q.empty()) {
    vector<uint128_t> *n;
    uint128_t hash_id;

    hash_id = q.front();
    q.pop();

    if (isEndGroup) {
      if ((hash_id & end) == hash_id)
        return true;
    } else if (hash_id == end)
      return true;

    n = OptGroup::nextGroupsHashID(hash_id);

    for (auto it = n->begin(); it != n->end(); it++) {
      q.push(*it);
    }

    delete n;
  }

  return false;
}

/**
 * Returns true if cycle is formed by the graph starting from g in the group
 * hash_id with next vertices of g provided as a parameter.
 **/
inline bool isCycle(uint128_t hash_id, uint128_t g, vector<uint128_t> *next) {
  for (auto it = next->begin(); it != next->end(); it++) {
    if (*it != g && reachable(*it, g, false))
      return true;
  }

  return false;
}

/**
 * Returns true if cycle is formed in group given by hash_id starting from
 * vertex g.
 **/
inline bool isCycle(uint128_t hash_id, uint128_t g) {
  vector<uint128_t> *next;

  next = OptGroup::nextGroupsHashID(g);
  for (auto it = next->begin(); it != next->end(); it++) {
    if (reachable(*it, hash_id, true))
      return true;
  }

  return false;
}

/**
 * Get all nodes from node to dest by following only next vertices.
 **/
void getAllNodesInNextPath(uint128_t node, uint128_t dest,
                           Uint128UnorderedSet &nodes_in_path,
                           vector<uint128_t> &path,
                           vector<uint128_t> *(*next_groups)(uint128_t)) {
  path.push_back(node);

  if (node == dest) {
    for (auto it = path.begin(); it != path.end(); it++) {
      nodes_in_path.insert(*it);
    }
  } else {
    vector<uint128_t> *n;

    n = next_groups(node);

    for (auto it = n->begin(); it != n->end(); it++) {
      if (nodes_in_path.find(*it) == nodes_in_path.end())
        getAllNodesInNextPath(*it, dest, nodes_in_path, path, next_groups);
    }

    delete n;
  }

  path.pop_back();
}

inline uint128_t getCycleGroup(uint128_t curr_group, uint128_t next_group) {
  uint128_t cg;
  Uint128UnorderedSet visited_curr_to_next;
  Uint128UnorderedSet visited_next_to_curr;
  vector<uint128_t> intersection;
  vector<uint128_t> *next_hashids;
  vector<uint128_t> path_vector;
  stack<uint128_t> _stack;

  cg = curr_group | next_group;
  next_hashids = OptGroup::nextGroupsHashID(curr_group);
  for (auto it = next_hashids->begin(); it != next_hashids->end(); it++) {
    if (*it != next_group)
      getAllNodesInNextPath(*it, next_group, visited_curr_to_next, path_vector,
                            OptGroup::nextGroupsHashID);
  }

  delete next_hashids;

  PRINT_DEBUG_BLOCK_L2 {
    std::cout << "getCycleGroup(): visited_curr_to_next = " << std::endl;

    for (auto it = visited_curr_to_next.begin();
         it != visited_curr_to_next.end(); it++) {
      std::cout << "   " << std::bitset<41>((uint64_t)*it) << std::endl;
    }
  }

  PRINT_DEBUG_BLOCK_L2 {
    std::cout << "getCycleGroup(): visited_next_to_curr = " << std::endl;

    for (auto it = visited_next_to_curr.begin();
         it != visited_next_to_curr.end(); it++) {
      std::cout << "   " << std::bitset<41>((uint64_t)*it) << std::endl;
    }
  }
  for (auto it = visited_curr_to_next.begin(); it != visited_curr_to_next.end();
       it++) {
    if (visited_next_to_curr.find(*it) != visited_next_to_curr.end())
      intersection.push_back(*it);
  }

  PRINT_DEBUG_BLOCK_L2 {
    std::cout << "getCycleGroup(): intersection = " << std::endl;

    for (auto it = intersection.begin(); it != intersection.end(); it++) {
      std::cout << "   " << std::bitset<41>((uint64_t)*it) << std::endl;
    }
  }

  for (auto it = visited_curr_to_next.begin(); it != visited_curr_to_next.end();
       it++) {
    cg |= *it;
  }

  return cg;
}

/**
 * Main Memoization function. Takes a starting hash_id and returns the minimum
 * cost.
 **/
inline uint64_t cfgMemoize(uint128_t hash_id) {
  uint64_t totalMinCost;
  vector<uint128_t> *n;

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "cfgMemoize(): STARTING for hash_id ";
    std::cout << std::bitset<41>((uint64_t)hash_id) << std::endl;
  }
  auto g = T.find(hash_id);
  if (g != T.end()) {
    PRINT_DEBUG_BLOCK_L2 {
      std::cout << "cfgMemoize(): found hash_id ";
      std::cout << std::bitset<41>((uint64_t)hash_id) << std::endl;
    }

    foundInDict += 1;

    return g->second;
  }

  PRINT_DEBUG_BLOCK_L2 {
    std::cout << "cfgMemoize(): Get next groups for ";
    std::cout << std::bitset<41>((uint64_t)hash_id) << std::endl;
  }

  n = OptGroup::nextGroupsHashID(hash_id);

  if (n->size() == 0) {
    // Last Group, i.e., no next groups available
    int max_dim;
    std::vector<uint64_t> tile_sizes;
    uint64_t _cost;

    delete n;
    max_dim = get_max_dimensions_for_group(hash_id);
    tile_sizes = std::vector<uint64_t>(max_dim, 0);
    hashIDToOptHashID[hash_id] = hash_id;
    _cost = cost(hash_id, tile_sizes);
    optHashIDToTileSizes[hash_id] = tile_sizes;
    T[hash_id] = _cost;

    if (hash_id && (!(hash_id & (hash_id - 1))))
      return _cost;

    return _cost;
  }

  runningTime += 1;

  if (numberOfOnes(hash_id) < (1UL << logMaxChildren)) {
    int max_dim_curr_group;
    uint64_t cost_curr_group;
    uint64_t cost_next_groups;
    std::vector<uint64_t> opt_tile_sizes;
    std::vector<uint64_t> opt_tile_sizes_next_group;
    std::vector<uint64_t> tile_sizes_curr_group;
    uint128_t optHashID;
    uint128_t next_hash_id;
    unordered_set<uint128_t, uint128Hasher> includedVertices;

    totalMinCost = 1L << 60;
    optHashID = 0;
    next_hash_id = 0;
    cost_next_groups = 0;

    // Calculate cost for next vertices without grouping
    for (auto it = n->begin(); it != n->end(); ++it) {
      std::vector<uint64_t> tile_sizes_next_group;
      int max_dim_next_group;
      uint64_t _cost;

      max_dim_next_group = get_max_dimensions_for_group(*it);
      tile_sizes_next_group = std::vector<uint64_t>(max_dim_next_group, 0);
      _cost = cfgMemoize(*it);
      cost_next_groups += _cost;
    }

    PRINT_DEBUG_BLOCK_L1 {
      std::cout << "cfgMemoize(): cost_next_groups = ";
      std::cout << cost_next_groups << std::endl;
    }

    max_dim_curr_group = get_max_dimensions_for_group(hash_id);
    tile_sizes_curr_group = std::vector<uint64_t>(max_dim_curr_group, 0);
    cost_curr_group = cost(hash_id, tile_sizes_curr_group);

    // Group with every next group and find total minimum cost
    for (auto it = n->begin(); it != n->end(); ++it) {
      uint64_t cost1;
      uint64_t cost2;
      std::vector<uint64_t> tile_sizes_cg;
      std::vector<uint64_t> tile_sizes_next_group;
      int max_dim_next_group;
      uint128_t cg;

      cg = (*it) | hash_id;
      PRINT_DEBUG_BLOCK_L1 {
        std::cout << "cfgMemoize(): it ";
        std::cout << std::bitset<41>((uint64_t)*it) << std::endl;
      }
      if (isCycle(hash_id, *it, n)) {
        PRINT_DEBUG_BLOCK_L2 {
          std::cout << "cfgMemoize(): Cycle Found in Grouping, Cannot Continue "
                    << std::endl;
        }
        cost1 = -1;
      } else if (isCycle(hash_id, *it)) {
        cg = getCycleGroup(hash_id, *it);
        cost1 = -1; // Invalid Grouping
      } else {
        int max_dim_cg;
        uint64_t cg_cost;

        PRINT_DEBUG_BLOCK_L2 {
          std::cout << "cfgMemoize(): max_dim_cg = " << max_dim_cg;
          std::cout << std::endl;
        }

        max_dim_cg = get_max_dimensions_for_group(cg);
        tile_sizes_cg = std::vector<uint64_t>(max_dim_cg, 0);
        cg_cost = cost(cg, tile_sizes_cg);

        PRINT_DEBUG_BLOCK_L2 {
          std::cout << "cfgMemoize(): cg_cost = " << (int64_t)cg_cost;
          std::cout << std::endl;
        }
        if ((int64_t)cg_cost == -1)
          cost1 = -1;
        else {
          uint64_t o = cfgMemoize(cg);
          cost1 = o;
        }
      }

      max_dim_next_group = get_max_dimensions_for_group(*it);
      tile_sizes_next_group = std::vector<uint64_t>(max_dim_next_group, 0);
      cost2 = cost_next_groups + cost_curr_group;
      cost(*it, tile_sizes_next_group);

      if (cost1 >= 0 && cost2 >= 0)
        totalMinCost = min(totalMinCost, min(cost1, cost2));
      else if (cost1 < 0) {
        totalMinCost = min(totalMinCost, cost2);
      } else if (cost2 < 0) {
        totalMinCost = min(totalMinCost, cost1);
      }

      PRINT_DEBUG_L1(std::cout
                     << "cfgMemoize(): hash_id = "
                     << std::bitset<41>((uint64_t)hash_id) << " totalmin "
                     << totalMinCost << " = " << std::bitset<41>((uint64_t)cg)
                     << " =  " << (int64_t)cost1 << "   " << cost2 << " = "
                     << std::bitset<41>((uint64_t)(*it)) << std::endl);
      if (cost1 == totalMinCost) {
        opt_tile_sizes = tile_sizes_cg;
        optHashID = cg;
      } else {
        opt_tile_sizes = tile_sizes_curr_group;
        opt_tile_sizes_next_group = tile_sizes_next_group;
        next_hash_id = *it;
      }
    }

    if (optHashID != 0) {
      hashIDToOptHashID[hash_id] = optHashID;
      optHashIDToTileSizes[optHashID] = opt_tile_sizes;
    }

    else {
      hashIDToOptHashID[hash_id] = hash_id;
      totalMinCost = cost_next_groups + cost_curr_group;
      optHashIDToTileSizes[hash_id] = opt_tile_sizes;
      optHashIDToTileSizes[next_hash_id] = opt_tile_sizes_next_group;
    }
  } else {
    // Current Group Size less than Max Group Size
    int max_dim_curr_group;
    std::vector<uint64_t> tile_sizes_next_group;

    totalMinCost = 0;
    for (auto it = n->begin(); it != n->end(); ++it) {
      uint64_t q;
      std::vector<uint64_t> tile_sizes_next_group;
      int max_dim_next_group;

      max_dim_next_group = get_max_dimensions_for_group(*it);
      tile_sizes_next_group = std::vector<uint64_t>(max_dim_next_group, 0);
      q = cfgMemoize(*it);
      totalMinCost += q + cost(*it, tile_sizes_next_group);
      optHashIDToTileSizes[*it] = tile_sizes_next_group;
    }

    max_dim_curr_group = get_max_dimensions_for_group(hash_id);
    tile_sizes_next_group = std::vector<uint64_t>(max_dim_curr_group, 0);
    totalMinCost += cost(hash_id, tile_sizes_next_group);
    hashIDToOptHashID[hash_id] = hash_id;
    optHashIDToTileSizes[hash_id] = tile_sizes_next_group;
  }

  T[hash_id] = totalMinCost;
  delete n;

  PRINT_DEBUG_L1(std::cout << "cfgMemoze(): ENDING for hash_id "
                           << std::bitset<41>((uint64_t)hash_id) << std::endl);

  return totalMinCost;
}

/**
 * Do topological sort of graph starting from vertex and return the result in
 * output.
 **/
void topological_sort(uint128_t vertex, vector<uint128_t> &output,
                      Uint128UnorderedSet &visited,
                      vector<uint128_t> *next_groups) {
  vector<uint128_t> *n;

  visited.insert(vertex);
  n = OptGroup::nextGroupsHashID(vertex);

  for (auto it = n->begin(); it != n->end(); it++) {
    if (visited.find(*it) == visited.end())
      topological_sort(*it, output, visited, next_groups);
  }

  if (next_groups == NULL)
    output.push_back(vertex);

  else if (std::find(next_groups->begin(), next_groups->end(), vertex) !=
           next_groups->end()) {
    output.push_back(vertex);
  }

  delete n;
}

bool insertingNextPrevCreatesCycle(OptGroup *g1, OptGroup *g2) {
  bool g1_in_g2_next;
  bool g1_in_g2_prev;
  bool g2_in_g1_next;
  bool g2_in_g1_prev;

  g1_in_g2_next = g2->nextGroups().find(g1) != g2->nextGroups().end();
  g1_in_g2_prev = true;
  g2_in_g1_next = g1->nextGroups().find(g2) != g1->nextGroups().end();
  g2_in_g1_prev = true;

  return g1_in_g2_next && g1_in_g2_prev && g2_in_g1_next && g2_in_g1_prev;
}

/**
 * Follow topological order of graph to form final grouping using the
 * minimum cost data provided in the dictionary.
 **/
OptGroup *opt_grouping_topological_sort(uint128_t _grp_hash_id) {
  vector<uint128_t> optgroup_vector;
  Uint128UnorderedSet visited;
  vector<uint128_t> topological_sort_output;

  topological_sort(_grp_hash_id, topological_sort_output, visited, NULL);
  reverse(topological_sort_output.begin(), topological_sort_output.end());

  // Iterate every function in topological sort of the pipeline.
  for (auto it = topological_sort_output.begin();
       it != topological_sort_output.end(); it++) {
    bool findInOptHashDict;
    OptGroup *optg;
    OptGroup *g;
    uint128_t hash_id;

    findInOptHashDict = false;

    // Find if the current vertex is already in a group found earlier by a
    // vertex
    for (auto it2 = optgroup_vector.begin(); it2 != optgroup_vector.end();
         it2++) {
      if (((*it2) & (*it)) == (*it)) {
        OptGroup *g;

        // If yes, then update current group
        g = OptGroup::optHashIDToGroup[*it];
        findInOptHashDict = true;
        for (auto it3 = g->prevGroups().begin(); it3 != g->prevGroups().end();
             it3++) {
          for (auto it4 = (*it3)->parentGroups().begin();
               it4 != (*it3)->parentGroups().end(); it4++) {
            OptGroup *optg;

            optg = OptGroup::parentOptGroupsToHashID[*it2];
            if (*it4 != optg && !insertingNextPrevCreatesCycle(*it4, optg)) {
              (*it4)->nextGroups().insert(optg);
              optg->prevGroups().insert(*it4);
            }
          }
        }
      }
    }

    if (findInOptHashDict == true)
      continue;

    hash_id = *it;
    // Find the biggest optimal group.
    while (hashIDToOptHashID.find(hash_id) != hashIDToOptHashID.end()) {
      if (hashIDToOptHashID[hash_id] == hash_id)
        break;

      hash_id = hashIDToOptHashID[hash_id];
    }

    optg = OptGroup::createParentOptGroup(hash_id);
    g = OptGroup::optHashIDToGroup[*it];

    // Update the group
    for (auto it3 = g->prevGroups().begin(); it3 != g->prevGroups().end();
         it3++) {
      for (auto it4 = (*it3)->parentGroups().begin();
           it4 != (*it3)->parentGroups().end(); it4++) {
        if (*it4 != optg && !insertingNextPrevCreatesCycle(*it4, optg)) {
          (*it4)->nextGroups().insert(optg);
          optg->prevGroups().insert(*it4);
        }
      }
    }

    optgroup_vector.push_back(hash_id);
  }

  // Return the group of first vertex
  return *(OptGroup::optHashIDToGroup[_grp_hash_id]->parentGroups().begin());
}

/**
 * Utility function for finding cycles.
 * Used in detect and destroy cycles.
 **/
bool isCyclicUtil(OptGroup *v, unordered_map<OptGroup *, bool> &visited,
                  unordered_map<OptGroup *, bool> &recStack, OptGroup **cycle,
                  int cycleIndex, int &cycle_length) {
  if (visited[v] == false) {
    // Mark the current node as visited and part of recursion stack
    visited[v] = true;
    recStack[v] = true;
    cycle[cycleIndex] = v;
    // Recur for all the vertices adjacent to this vertex
    for (auto &iter : v->nextGroups()) {
      cycle_length++;
      if (!visited[(OptGroup *)iter] &&
          isCyclicUtil((OptGroup *)iter, visited, recStack, cycle,
                       cycleIndex + 1, cycle_length)) {
        return true;
      } else if (recStack[(OptGroup *)iter]) {
        return true;
      }
      cycle_length--;
    }
  }

  recStack[v] = false; // remove the vertex from recursion stack
  return false;
}

/**
 * Detect and remove cycles if found in graph.
 **/
void detect_and_destroy_cycles() {
  bool change;

  change = true;

  while (change) {
    // Mark all the vertices as not visited and not part of recursion
    // stack
    static int n_calls = 0;
    int n_vertices;
    int cycle_length;
    int q;
    OptGroup **cycle;
    OptGroup *child_to_disconnect;
    OptGroup *curr_grp;
    OptGroup *next_grp;
    OptGroup *to_remove;
    unordered_map<OptGroup *, bool> visited;
    unordered_map<OptGroup *, bool> recStack;

    q = 0;
    cycle_length = 0;
    change = false;
    child_to_disconnect = NULL;
    n_vertices = OptGroup::parentOptGroupsToHashID.size();
    cycle = new OptGroup *[n_vertices];

    for (auto const &iter : OptGroup::parentOptGroupsToHashID) {
      visited[iter.second] = false;
      recStack[iter.second] = false;
    }
    n_calls++;
    // Call the recursive helper function to detect cycle in different
    // DFS trees
    for (auto const &iter : OptGroup::parentOptGroupsToHashID) {
      q++;
      cycle_length = 0;

      if (isCyclicUtil(iter.second, visited, recStack, cycle, 0,
                       cycle_length)) {
        change = true;
        break;
      }
    }

    if (!change)
      continue;

    // Break cycle by creating separate group of child which is
    // not the input (of group) and has an edge from another group

    for (int i = 0; i < cycle_length; i++) {
      int curr_grp_index;
      int next_grp_index;

      curr_grp_index = i;
      next_grp_index = (i + 1) % cycle_length;
      curr_grp = cycle[curr_grp_index];
      next_grp = cycle[next_grp_index];

      // Find a child in next_grp having an edge from a child of curr_grp
      // and is not the input of the next_grp
      for (auto const &curr_child : curr_grp->childGroups()) {
        for (auto const &next_curr_child : curr_child->nextGroups()) {
          if ((next_curr_child->hashID() & next_grp->hashID()) ==
              (next_curr_child->hashID())) {
            bool candidate_is_input;

            candidate_is_input = false;

            for (auto const &prev : next_curr_child->prevGroups()) {
              if ((prev->hashID() & next_grp->hashID()) == prev->hashID()) {
                candidate_is_input = true;
                break;
              }
            }

            if (candidate_is_input)
              child_to_disconnect = (OptGroup *)next_curr_child;
          }

          if (child_to_disconnect)
            break;
        }

        if (child_to_disconnect)
          break;
      }

      if (child_to_disconnect)
        break;
    }

    to_remove = OptGroup::createParentOptGroup(child_to_disconnect->hashID());

    assert(to_remove != child_to_disconnect);

    for (auto const &_next_grp : child_to_disconnect->nextGroups()) {
      for (auto const &grp : ((OptGroup *)_next_grp)->parentGroups()) {
        if (grp->hashID() != next_grp->hashID())
          to_remove->nextGroups().insert(grp);
      }
    }

    for (auto const &_next_grp : to_remove->nextGroups()) {
      if (_next_grp->hashID() != next_grp->hashID())
        _next_grp->prevGroups().insert((OptGroup *)to_remove);
    }

    for (auto const &prev_grp : child_to_disconnect->prevGroups()) {
      for (auto const &grp : ((OptGroup *)prev_grp)->parentGroups()) {
        if (grp->hashID() != next_grp->hashID())
          to_remove->prevGroups().insert(grp);
      }
    }

    for (auto const &prev_grp : to_remove->prevGroups()) {
      if (prev_grp->hashID() != next_grp->hashID())
        prev_grp->nextGroups().insert((OptGroup *)to_remove);
    }

    // Insert to_remove in prevGroups and nextGroups of
    next_grp->removeChild(to_remove->hashID(), to_remove);

    delete cycle;
  }
}

void correct_grouping() {
  bool change;

  change = true;

  while (change) {
    change = false;
    for (auto it = OptGroup::optHashIDToGroup.begin();
         it != OptGroup::optHashIDToGroup.end(); it++) {
      OptGroup *group;
      OptGroup *minParentGroup;
      uint64_t minCost;

      vector<OptGroup *> toRemoveParents;

      group = it->second;

      if (group == NULL)
        continue;

      if (group != NULL && group->parentGroups().size() <= 1)
        continue;

      change = true;
      minParentGroup = nullptr;
      minCost = 1L << 60;

      for (auto it = group->parentGroups().begin();
           it != group->parentGroups().end(); it++) {
        int max_dim;
        uint64_t _cost;
        std::vector<uint64_t> tile_sizes;

        max_dim = get_max_dimensions_for_group((*it)->hashID());
        tile_sizes = std::vector<uint64_t>(max_dim, 0);
        _cost = cost((*it)->hashID(), tile_sizes);

        if (_cost < minCost &&
            !((*it)->hashID() &&
              (!((*it)->hashID() & ((*it)->hashID() - 1))))) {
          minCost = _cost;
          minParentGroup = (*it);
        }
      }

      /* removeChild () modifies the child's parentGroups() set.
       * Hence, we cannot call removeChild () from the loop iterator
       * of child's parentGroups ().
       * Create a copy of parents and call removeChild on them.
       **/

      for (auto it = group->parentGroups().begin();
           it != group->parentGroups().end(); it++) {
        if (*it != minParentGroup) {
          toRemoveParents.push_back(*it);
        }
      }

      for (auto it = toRemoveParents.begin(); it != toRemoveParents.end();
           it++) {
        (*it)->removeChild(group->hashID(), minParentGroup);
      }
    }
  }

  detect_and_destroy_cycles();
}

/**
 * Get first least significant pipe.Group in hash_id
 **/
PyObject *firstPyGroup(uint128_t hash_id, bool dummySource, uint64_t maxID) {
  uint64_t bit;

  bit = 0;

  while (hash_id != 0) {
    if ((hash_id & 1L) == 1) {
      uint128_t l;
      Group *g;
      PyObject *o;

      l = 1;
      l = l << bit;
      g = Group::hashIDToGroup[l];
      o = g->getPyGroup();
      if (o != NULL)
        return o;
    }

    hash_id = hash_id >> 1;
    bit++;
  }

  return NULL;
}

PyObject *getPyGroupTopologicalOrder(PyObject *pyg) {
  PyObject *order;
  order = PyDict_GetItem(pygroup_topological_order, pyg);
  return order;
}

/**
 * Sort pipe.Group using topological order.
 **/
bool pyGroupSortFunc(PyObject *pyg1, PyObject *pyg2) {
  return PyLong_AsLong(getPyGroupTopologicalOrder(pyg1)) <
         PyLong_AsLong(getPyGroupTopologicalOrder(pyg2));
}

/**
 * Clear all book keeping data structures.
 **/
void clear_everything() {
  vector<std::unordered_map<uint128_t, OptGroup *, uint128Hasher>::iterator>
      toremove;

  hashIDToOptHashID.clear();
  T.clear();
  OptGroup::optHashIDToGroup.clear();
  OptGroup::vectorOptGroups.clear();

  for (auto it = OptGroup::parentOptGroupsToHashID.begin();
       it != OptGroup::parentOptGroupsToHashID.end(); it++) {
    OptGroup::optHashIDToGroup[it->first] = it->second;
    OptGroup::vectorOptGroups.push_back(it->second);
  }

  OptGroup::parentOptGroupsToHashID.clear();

  optHashIDToTileSizes.clear();
}

/**
 * Main function of the library. Arguments are
 * in_group: A list of input groups
 * out_group: A list of output groups
 * groups: A list of all groups
 * pipeline: The Pipeline Object
 * reduction_cls: Class object of Reduction class
 * small_comps: A list of small computations
 * comp_size_map: A map of each computation to size
 * tstencil_cls: Class object of TStencil Class
 * pygroup_topological_order: Topological Order of Groups
 * pygroup_dim_reuse: Dimension reuse across each dimension for each group
 * pylive_size:
 * pydim_size: Dimension sizes of each dimension of each group
 * storage_mapping_get_dim_size: Object for function
 * storage_mapping.get_dim_size cls_Storage: Class Object of Storage Class
 */
PyObject *dpgroup(PyObject *self, PyObject *args) {
  bool dummySource;
  PyObject *in_group;
  PyObject *out_group;
  PyObject *groups;
  PyObject *do_inline;
  PyObject *multi_level_tiling;
  PyObject *_printDPFusionTimeAndChoices;
  int _logMaxChildren;
  Group *start;
  OptGroup *opt_start;
  PyObject *grp_size_o;
  uint64_t max_group_size;
  uint128_t start_hash_id;
  int iteration;
  uint64_t _maxID;
  clock_t t1;
  uint64_t dpChoices;
  std::vector<PyObject *> pygroups_vector;

  std::cerr << "--------DP Fusion--------" << std::endl;

#ifdef GDB_DEBUG
  std::cout << "Press Enter after connecting GDB" << std::endl;
  getchar();
#endif


  // There is a bug (I think it is a bug) in Python3.6m C interface library,
  // when PyList_* are applied on the the first argument (in_group here) 
  // then it results in seg faults.
  // As a hack, reinitialize in_group as args is a tuple of arguments passed.
  int r = PyArg_ParseTuple(
      args, "OOOOOOOOOOOOOOOOllllffffOl", &in_group, &out_group, &groups,
      &pipeline, &reduction_cls, &small_comps, &comp_size_map, &tstencil_cls,
      &pygroup_topological_order, &pygroup_dim_reuse, &pylive_size, &pydim_size,
      &storage_mapping_get_dim_size, &cls_Storage,
      // Optimization Parameters (Inlining, MultiLevelTiling)
      &do_inline, &multi_level_tiling,
      // Machine Info
      &L1_CACHE_SIZE, &L2_CACHE_SIZE, &N_CORES, &IMAGE_ELEMENT_SIZE,
      // Weights
      &DIM_STD_DEV_WEIGHT, &LIVE_SIZE_TO_TILE_SIZE_WEIGHT,
      &CLEANUP_THREADS_WEIGHT, &RELATIVE_OVERLAP_WEIGHT,
      &_printDPFusionTimeAndChoices, &_logMaxChildren);
  
  in_group = PyTuple_GetItem (args, 0);
  assert (r == 1);
  check_and_print_exception ();

  std::cerr << "Machine Information:" << std::endl
            << "L1_CACHE_SIZE       " << L1_CACHE_SIZE << " Bytes" << std::endl
            << "L2_CACHE_SIZE       " << L2_CACHE_SIZE << " Bytes" << std::endl
            << "N_CORES             " << N_CORES << std::endl
            << "IMAGE_ELEMENT_SIZE  " << IMAGE_ELEMENT_SIZE << std::endl
            << std::endl;

  std::cerr << "Weights: " << std::endl
            << "DIM_STD_DEV_WEIGHT            " << DIM_STD_DEV_WEIGHT
            << std::endl
            << "LIVE_SIZE_TO_TILE_SIZE_WEIGHT " << LIVE_SIZE_TO_TILE_SIZE_WEIGHT
            << std::endl
            << "CLEANUP_THREADS_WEIGHT        " << CLEANUP_THREADS_WEIGHT
            << std::endl
            << "RELATIVE_OVERLAP_WEIGHT       " << RELATIVE_OVERLAP_WEIGHT
            << std::endl
            << std::endl;

  get_overlapping_size_func = PyObject_GetAttr(
      pipeline, Py_BuildValue("s", "get_overlapping_size_for_groups"));
  if (_printDPFusionTimeAndChoices == Py_True) {
    printDPFusionTimeAndChoices = true;
    logMaxChildren = _logMaxChildren;
    log_increment_factor = 10;
  } else
    printDPFusionTimeAndChoices = false;

  if (do_inline == Py_True)
    INLINING_ENABLED = true;
  else if (do_inline == Py_False)
    INLINING_ENABLED = false;
  else
    assert(false);

  if (multi_level_tiling == Py_True)
    MULTI_LEVEL_TILING_ENABLED = true;
  else if (multi_level_tiling == Py_False)
    MULTI_LEVEL_TILING_ENABLED = false;
  else
    assert(false);

  if (INLINING_ENABLED)
    std::cerr << "INLINING           ENABLED" << std::endl;
  else
    std::cerr << "INLINING           DISABLED" << std::endl;

  if (MULTI_LEVEL_TILING_ENABLED)
    std::cerr << "MULTI_LEVEL_TILING ENABLED" << std::endl;
  else
    std::cerr << "MULTI_LEVEL_TILING DISABLED" << std::endl;

  std::cerr << std::endl << "-----Running DPFusion-----" << std::endl;
  check_and_print_exception ();
  if (PyList_Size(in_group) == 1) {
    maxID = PyList_Size(groups);
    dummySource = false;
  } else {
    maxID = PyList_Size(groups) + 1;
    dummySource = true;
  }

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "Total Number of Nodes " << maxID << std::endl;
  }

  for (int i = 0; i < PyList_Size(small_comps); i++) {
    PyObject *comp = PyList_GetItem(small_comps, i);

    small_comps_set.insert(comp);
  }

  for (int i = 0; i < PyList_Size(groups); i++) {
    pygroups_vector.push_back(PyList_GetItem(groups, i));
  }

  sort(pygroups_vector.begin(), pygroups_vector.end(), pyGroupSortFunc);

  // Get All Data from Python
  for (uint64_t i = 0; i < pygroups_vector.size(); i++) {
    PyObject *group;
    PyObject *comps;
    Group *cppGroup;

    group = pygroups_vector[i];
    comps = PyObject_GetAttr(group, Py_BuildValue("s", "comps"));

    for (int j = 0; j < PyList_Size(comps); j++) {
      static PyObject *str_lookup_key = Py_BuildValue("s", "lookup_key");
      static PyObject *str_orig_stg_class =
          Py_BuildValue("s", "orig_storage_class");
      static PyObject *str_func = Py_BuildValue("s", "func");
      static PyObject *str_typ = Py_BuildValue("s", "typ");
      static PyObject *str_ndims = Py_BuildValue("s", "ndims");
      static PyObject *str_offsets = Py_BuildValue("s", "offsets");
      PyObject *storage;
      PyObject *key;
      PyObject *comp;
      PyObject *func;
      PyObject *offsets;
      PyObject *typ;
      PyObject *dims;
      vector<long> max_offset;

      comp = PyList_GetItem(comps, j);
      func = PyObject_GetAttr(comp, str_func);
      typ = PyObject_GetAttr(func, str_typ);
      dims = PyObject_GetAttr(func, str_ndims);
      storage = PyObject_GetAttr(comp, str_orig_stg_class);
      key = PyObject_GetAttr(storage, str_lookup_key);
      string _strkey = pystring_to_string(key);
      offsets = PyObject_GetAttr(storage, str_offsets);

      pyGroupForComp[comp] = group;
      comp_to_orig_storage[comp] = storage;
      stg_class_to_key[storage] = _strkey;
      comp_to_func[comp] = func;
      func_to_typ[func] = typ;
      func_to_ndims[func] = PyLong_AsLong(dims);

      for (long i = 0; i < PyLong_AsLong(dims); i++) {
        PyObject *_max_offset = PyTuple_GetItem(PyList_GetItem(offsets, i), 1);
        max_offset.push_back(PyLong_AsLong(_max_offset));
      }

      comp_to_max_offsets[comp] = max_offset;
      helper_storage_to_dim_storage[comp] = std::vector<PyObject *>();

      for (long dim = 0; dim < PyLong_AsLong(dims); dim++) {
        static PyObject *str_get_dim = Py_BuildValue("s", "get_dim");
        PyObject *dim_storage;
        PyObject *get_dim;
        PyObject *args;

        get_dim = PyObject_GetAttr(storage, str_get_dim);
        args = PyTuple_Pack(1, PyLong_FromLong(dim));
        dim_storage = PyObject_CallObject(get_dim, args);
        Py_DECREF(args);

        helper_storage_to_dim_storage[storage].push_back(dim_storage);
      }
    }

    cppGroup = new Group(group, comps);
    pyToGroup[group] = cppGroup;
    OptGroup::createOptGroup(cppGroup->hashID());
    PRINT_DEBUG_BLOCK_L1 {
      PyObject *name;
      PyObject *pyStr;
      const char *ss;

      name = PyObject_GetAttr(group, Py_BuildValue("s", "name"));
      pyStr = PyUnicode_AsEncodedString(name, "utf-8", "Error ~");
      ss = PyBytes_AS_STRING(pyStr);
      std::cout << "hash_id ";
      std::cout << std::bitset<41>((uint64_t)cppGroup->hashID()) << "  ";
      std::cout << ss << std::endl;
    }
  }

  // Create the DAG
  for (int i = 0; i < PyList_Size(groups); i++) {
    PyObject *group;
    Group *cppGroup;
    OptGroup *optGroup;
    PyObject *children;
    PyObject *parents;

    group = PyList_GetItem(groups, i);
    cppGroup = pyToGroup[group];
    optGroup = OptGroup::optHashIDToGroup[cppGroup->hashID()];
    children = PyObject_GetAttr(group, Py_BuildValue("s", "children"));

    for (int j = 0; j < PyList_Size(children); j++) {
      PyObject *child;
      Group *cppChild;
      OptGroup *optChild;

      child = PyList_GetItem(children, j);
      cppChild = pyToGroup[child];
      cppGroup->nextGroups().insert(cppChild);
      optChild = OptGroup::optHashIDToGroup[cppChild->hashID()];
      optGroup->nextGroups().insert(optChild);
    }

    parents = PyObject_GetAttr(group, Py_BuildValue("s", "parents"));

    for (int j = 0; j < PyList_Size(parents); j++) {
      PyObject *parent;
      Group *cppParent;
      OptGroup *optParent;

      parent = PyList_GetItem(parents, j);
      cppParent = pyToGroup[parent];
      cppGroup->prevGroups().insert(cppParent);
      optParent = OptGroup::optHashIDToGroup[cppParent->hashID()];
      optGroup->prevGroups().insert(optParent);
    }
  }

  // Get the start group
  if (PyList_Size(in_group) == 1) {
    start = pyToGroup[PyList_GetItem(in_group, 0)];
    opt_start = OptGroup::optHashIDToGroup[start->hashID()];
  } else {
    // More than one start vertex
    // Create a dummy group
    start = new Group();
    opt_start = OptGroup::createOptGroup(start->hashID());

    for (int i = 0; i < PyList_Size(in_group); i++) {
      Group *next;
      OptGroup *optnext;

      next = pyToGroup[PyList_GetItem(in_group, i)];
      start->nextGroups().insert(next);
      next->prevGroups().insert(start);
      optnext = OptGroup::optHashIDToGroup[next->hashID()];
      opt_start->nextGroups().insert(optnext);
      optnext->prevGroups().insert(opt_start);
    }
  }

  grp_size_o = PyObject_GetAttr(pipeline, Py_BuildValue("s", "_group_size"));
  grp_size = PyLong_AsLong(grp_size_o);
  max_group_size = 1UL << logMaxChildren;
  start_hash_id = opt_start->hashID();
  iteration = 0;
  _maxID = maxID;

  if (_maxID && (!(_maxID & (_maxID - 1)))) {
  } else {
    // Next power of 2 of maxID
    _maxID--;
    _maxID |= _maxID >> 1;
    _maxID |= _maxID >> 2;
    _maxID |= _maxID >> 4;
    _maxID |= _maxID >> 8;
    _maxID |= _maxID >> 16;
    _maxID++;
  }

  t1 = clock();
  dpChoices = 0;

  // Bounded Grouping Loop
  while (iteration == 0 || max_group_size <= _maxID) {
    if (iteration != 0)
      clear_everything();

    cfgMemoize(start_hash_id);
    dpChoices += T.size();

    for (auto it = hashIDToOptHashID.begin(); it != hashIDToOptHashID.end();
         it++) {
      PRINT_DEBUG_L2(std::cout << std::bitset<41>((uint64_t)it->first) << "    "
                               << std::bitset<41>((uint64_t)it->second)
                               << std::endl);
    }

    opt_start = opt_grouping_topological_sort(start_hash_id);
    PRINT_DEBUG_BLOCK_L1 {
      std::cout << "Bound Grouping iteration " << iteration;
      std::cout << " completed" << std::endl;
      std::cout << "Next Groups" << std::endl;
      for (auto it = OptGroup::parentOptGroupsToHashID.begin();
           it != OptGroup::parentOptGroupsToHashID.end(); it++) {
        if (it->second == NULL)
          continue;

        std::cout << std::bitset<41>((uint64_t)it->second->hashID());
        std::cout << std::endl;
        for (auto it2 = it->second->nextGroups().begin();
             it2 != it->second->nextGroups().end(); it2++) {
          std::cout << "        ";
          std::cout << std::bitset<41>((uint64_t)(*it2)->hashID());
          std::cout << std::endl;
        }
      }

      std::cout << "Prev Groups" << std::endl;

      for (auto it = OptGroup::parentOptGroupsToHashID.begin();
           it != OptGroup::parentOptGroupsToHashID.end(); it++) {
        if (it->second == NULL)
          continue;

        std::cout << std::bitset<41>((uint64_t)it->second->hashID());
        std::cout << std::endl;
        for (auto it2 = it->second->prevGroups().begin();
             it2 != it->second->prevGroups().end(); it2++) {
          std::cout << "        ";
          std::cout << std::bitset<41>((uint64_t)(*it2)->hashID());
          std::cout << std::endl;
        }
      }
    }

    correct_grouping();

    PRINT_DEBUG_BLOCK_L1 {
      std::cout << "Corrected Grouping" << std::endl;
      std::cout << "Corrected Next Groups" << std::endl;
      for (auto it = OptGroup::parentOptGroupsToHashID.begin();
           it != OptGroup::parentOptGroupsToHashID.end(); it++) {
        if (it->second == NULL)
          continue;

        std::cout << std::bitset<41>((uint64_t)it->second->hashID());
        std::cout << std::endl;
        for (auto it2 = it->second->nextGroups().begin();
             it2 != it->second->nextGroups().end(); it2++) {
          std::cout << "        ";
          std::cout << std::bitset<41>((uint64_t)(*it2)->hashID());
          std::cout << std::endl;
        }
      }

      std::cout << "Correct Prev Groups:" << std::endl;
      for (auto it = OptGroup::parentOptGroupsToHashID.begin();
           it != OptGroup::parentOptGroupsToHashID.end(); it++) {
        if (it->second == NULL)
          continue;

        std::cout << std::bitset<41>((uint64_t)it->second->hashID());
        std::cout << std::endl;
        for (auto it2 = it->second->prevGroups().begin();
             it2 != it->second->prevGroups().end(); it2++) {
          std::cout << "        ";
          std::cout << std::bitset<41>((uint64_t)(*it2)->hashID());
          std::cout << std::endl;
        }
      }
    }
    start_hash_id = opt_start->hashID();
    iteration++;
    logMaxChildren += log_increment_factor;
    max_group_size = 1 << logMaxChildren;
  }

  if (printDPFusionTimeAndChoices) {
    clock_t t2;

    t2 = clock();

    std::cout << "Time taken by DP-Fusion: "
              << (t2 - t1) * 1000.0 / CLOCKS_PER_SEC;
    std::cout << " ms" << std::endl;
    std::cout << "Number of choices evaluated by DP-Fusion: " << dpChoices;
    std::cout << std::endl;
    exit(EXIT_SUCCESS);
  }

  PRINT_DEBUG_L1(std::cin >> iteration);

  PRINT_DEBUG_BLOCK_L1 {
    std::cout << "Updating Grouping Back to Python" << std::endl;
  }

  queue<OptGroup *> opt_queue;
  OptGroup *opt_temp;
  PyObject *merge_groups_func;
  std::unordered_map<uint128_t, bool, uint128Hasher> visited;
  std::unordered_set<OptGroup *> done;

  opt_temp = opt_start;
  opt_queue.push(opt_temp);
  merge_groups_func =
      PyObject_GetAttr(pipeline, Py_BuildValue("s", "merge_groups"));

  visited[opt_temp->hashID()] = true;
  // Perform final grouping
  while (opt_queue.size() != 0) {
    // Traverse groups in BFS order
    InlinedAdjacentType new_nextGroups;
    InlinedAdjacentType new_prevGroups;
    PyObject *new_grp;

    opt_temp = opt_queue.front();
    opt_queue.pop();
    new_grp = firstPyGroup(opt_temp->hashID(), dummySource, maxID);

    if (done.find(opt_temp) == done.end() && opt_temp->hashID() != 0) {
      uint64_t first;

      first = first_one(opt_temp->hashID());
      done.insert(opt_temp);

      if (numberOfOnes(opt_temp->hashID()) > 1) {
        uint64_t bit;
        uint128_t hash_id;
        Group *g;
        uint128_t l;

        bit = 0;
        l = 1;
        hash_id = opt_temp->hashID();

        while (bit <= first) {
          hash_id = hash_id >> 1;
          bit++;
        }

        l = l << first;

        g = Group::hashIDToGroup[l];
        new_nextGroups[g] = OrderedGroupSet(g->nextGroups());
        new_prevGroups[g] = OrderedGroupSet(g->prevGroups());

        while (hash_id != 0) {
          if ((hash_id & 1L) == 1 && !(dummySource && bit == maxID - 1)) {
            PyObject *pp;
            PyObject *args;

            l = 1;
            l = l << bit;
            g = Group::hashIDToGroup[l];
            pp = g->getPyGroup();

            if (pp == NULL)
              continue;

            args = PyTuple_Pack(2, new_grp, pp);
            new_grp = PyObject_CallObject(merge_groups_func, args);
            new_nextGroups[g] = OrderedGroupSet(g->nextGroups());
            new_prevGroups[g] = OrderedGroupSet(g->prevGroups());

            if (PyErr_Occurred() != NULL) {
              PyErr_Print();
            }
          }

          hash_id = hash_id >> 1;
          bit++;
        }
      }
    }

    if (new_grp) {
      static PyObject *set_total_used_size =
          Py_BuildValue("s", "set_total_used_size");
      static PyObject *set_n_buffers = Py_BuildValue("s", "set_n_buffers");
      static PyObject *set_inline_comps =
          Py_BuildValue("s", "set_inline_comps");
      PyObject *set_total_used_size_func;
      PyObject *set_n_buffers_func;
      PyObject *set_inline_comps_func;
      PyObject *args;
      UnorderedGroupSet inlined_groups;
      uint64_t liveouts_size;
      uint64_t livein_size;
      uint64_t total_size_used;
      int n_buffers;

      n_buffers = 0;

      if (INLINING_ENABLED)
        update_graph_with_inlining(inlined_groups, new_nextGroups,
                                   new_prevGroups, opt_temp->hashID());

      total_size_used = getTotalSizeUsed(
          opt_temp->hashID(), n_buffers, liveouts_size, livein_size,
          inlined_groups, new_nextGroups, new_prevGroups);
      set_total_used_size_func = PyObject_GetAttr(new_grp, set_total_used_size);
      args = PyTuple_Pack(1, PyLong_FromLong(total_size_used));
      PyObject_CallObject(set_total_used_size_func, args);
      Py_DECREF(args);

      set_n_buffers_func = PyObject_GetAttr(new_grp, set_n_buffers);
      args = PyTuple_Pack(1, PyLong_FromLong(n_buffers));
      PyObject_CallObject(set_n_buffers_func, args);
      Py_DECREF(args);

      PyObject *list_inline_comps = PyList_New(0);

      for (auto const &it : inlined_groups) {
        PyList_Append(list_inline_comps, it->getCompAtIndex(0));
      }

      set_inline_comps_func = PyObject_GetAttr(new_grp, set_inline_comps);
      args = PyTuple_Pack(1, list_inline_comps);
      PyObject_CallObject(set_inline_comps_func, args);
      Py_DECREF(list_inline_comps);
      Py_DECREF(args);
    }

    for (auto it = opt_temp->nextGroups().begin();
         it != opt_temp->nextGroups().end(); it++) {
      if (visited.find(((*it)->hashID())) == visited.end())
        visited[(*it)->hashID()] = false;

      if (visited[(*it)->hashID()] == false) {
        visited[opt_temp->hashID()] = true;
        opt_queue.push((OptGroup *)*it);
      }
    }
  }

  // Clearing
  pyGroupForComp.clear();
  pyToGroup.clear();
  OptGroup::parentOptGroupsToHashID.clear();
  hashIDToOptHashID.clear();
  Group::hashIDToGroup.clear();
  T.clear();
  OptGroup::optHashIDToGroup.clear();
  small_comps_set.clear();
  optHashIDToTileSizes.clear();
  pyGroupForComp.clear();
  globalID = -1;

  PRINT_DEBUG_BLOCK_L1 { std::cout << "DP Fusion Completed" << std::endl; }

  return Py_BuildValue("s", "_group_size");
}

PyMethodDef module_methods[] = {{"dpgroup", dpgroup, METH_VARARGS},
                                {NULL, NULL}};

struct PyModuleDef dpgroupModule = {
    PyModuleDef_HEAD_INIT, "dpfusion", /* name of module */
    "",                                /* module documentation, may be NULL */
    -1, /* size of per-interpreter state of the module, or
           -1 if the module keeps state in global variables. */
    module_methods};

/*
 * Python calls this to let us initialize our module
 */
PyMODINIT_FUNC PyInit_dpfusion() { return PyModule_Create(&dpgroupModule); }
