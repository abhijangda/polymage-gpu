#include <iostream>
#include <Python.h>
#include <unordered_map> 
#include <unordered_set>
#include <set>
#include <algorithm>
#include <queue>
#include <regex>
#include <fstream>
#include <string>
#include <boost/multiprecision/cpp_int.hpp> 
#include <functional> 
#include <cctype>
#include <locale>
#include <math.h>
#include <map>
#include <numeric>

// Print only if DEBUG level is more than i
#define PRINT_DEBUG_L1(x)                                                      \
  {                                                                            \
    if (DEBUG != 0 && DEBUG <= 1) {                                            \
      x;                                                                       \
    }                                                                          \
  }
#define PRINT_DEBUG_BLOCK_L1 if (DEBUG != 0 && DEBUG <= 1)
#define PRINT_DEBUG_BLOCK_L2 if (DEBUG != 0 && DEBUG <= 2)
#define PRINT_DEBUG_BLOCK_L3 if (DEBUG != 0 && DEBUG <= 3)
#define PRINT_DEBUG_L2(x)                                                      \
  {                                                                            \
    if (DEBUG != 0 && DEBUG <= 2) {                                            \
      x;                                                                       \
    }                                                                          \
  }
#define PRINT_DEBUG_L3(x)                                                      \
  {                                                                            \
    if (DEBUG != 0 && DEBUG <= 3) {                                            \
      x;                                                                       \
    }                                                                          \
  }
/**DEBUG Levels
 * Highest Level Print all messages below that level
 * No Print level 0
 **/

using boost::multiprecision::uint128_t;
using namespace boost;
using namespace std;

#ifndef __GROUP_H__
#define __GROUP_H__

uint64_t globalID = -1, runningTime = 0, maxID = 0, foundInDict = 0;

struct uint128Hasher
{
    uint64_t operator()(const uint128_t i) const
    {
        return (uint64_t)i;
    }
};

struct uint128Comparer
{
    uint64_t operator()(const uint128_t i, const uint128_t j) const
    {
        return i < j;
    }
};

class OptGroup;

struct OptGroupComparer 
{
    bool operator() (const OptGroup* g1, const OptGroup* g2) const;
};


class Group 
{
public:
    struct GroupComparer
    {
        bool operator()(const Group* g1, const Group* g2) const
        {
            return g1->hashID() < g2->hashID();
        }
    };
    
    struct GroupHasher
    {
        uint64_t operator()(const Group* g1) const
        {
            return (uint64_t)g1->hashID();
        }
    };
    
protected:
    uint64_t id;
    uint128_t hash_id;
    vector<uint128_t> next_groups_int;
    vector<uint128_t> prev_groups_int;
    bool is_last;
    PyObject* pyGroup;
    PyObject* comps;
    
    set<Group*, Group::GroupComparer> next_groups, prev_groups;
    set<OptGroup*, OptGroupComparer> parent_groups;
    
public:
    static std::unordered_map<uint128_t, Group*, uint128Hasher> hashIDToGroup;
    
    Group ()
    {
        setHashID ();
        pyGroup = NULL;
        comps = NULL;
    }
    
    Group (PyObject* pyGroup, PyObject* comps)
    {
        this->pyGroup = pyGroup;
        this->comps = comps;
        setHashID ();
    }
    
    Group (uint128_t hash_id)
    {
        setHashID (hash_id);
        pyGroup = NULL;
        comps = NULL;
    }
    
    Group (set<Group*, GroupComparer>& next_groups, set<Group*, GroupComparer>& prev_groups)
    {
        this->next_groups = next_groups;
        this->prev_groups = prev_groups;
        if (nextGroups().size() == 0)
            is_last = true;
        else
            is_last = false;
        
        id = ++globalID;
        setHashID ();
    }
    
    void setHashID ()
    {
        id = ++globalID;
        if (id < maxID)
        {
            hash_id = 1L;
            hash_id = (hash_id << id);
            hashIDToGroup[hash_id] = this;
        }
    }
    
    PyObject* getPyGroup ()
    {
        return pyGroup;
    }
    
    PyObject* getComps ()
    {
        return comps;
    }
    
    PyObject* getCompAtIndex (int i)
    {
        return PyList_GetItem (comps, i);
    }
    
    set<OptGroup*, OptGroupComparer>& parentGroups()
    {
        return parent_groups;
    }
    
    set<Group*, GroupComparer>& nextGroups()
    {
        return next_groups;
    }
    
    set<Group*, GroupComparer>& prevGroups ()
    {
        return prev_groups;
    }
    
    void setHashID (uint128_t _hash_id)
    {
        hash_id = _hash_id;
    }
    
    uint128_t hashID () const
    {
        return hash_id;
    }
    
    bool isSingleGroup () const
    {
        if (hash_id && (!(hash_id & (hash_id-1))))
            return true;
        
        return false;
    }
    
    vector<uint128_t>* prevGroupsHashID ()
    {
        if (prev_groups_int.size () != 0)
        {
            return &prev_groups_int;
        }
        
        for (auto it = prevGroups().begin(); it != prevGroups().end(); ++it)
        {
            prev_groups_int.push_back ((*it)->hashID());
        }
         
        return &prev_groups_int;
    }
    
    vector<uint128_t>* nextGroupsHashID ()
    {
        if (next_groups_int.size () != 0)
        {
            return &next_groups_int;
        }
        
        for (auto it = nextGroups().begin(); it != nextGroups().end(); ++it)
        {
            next_groups_int.push_back ((*it)->hashID());
        }
         
        return &next_groups_int;
    }
    
    
    bool operator==(const Group& other) const
    {
        return other.hash_id == hash_id;
    }
};

class OptGroup : public Group
{
protected:
    set<OptGroup*, Group::GroupComparer> children;
    
    OptGroup (uint128_t hash_id) : Group (hash_id)
    {
        //setChildren ();
    }

public:   
    static std::unordered_map<uint128_t, OptGroup*, uint128Hasher> optHashIDToGroup;
    static std::unordered_map<uint128_t, OptGroup*, uint128Hasher> parentOptGroupsToHashID;
    static vector <OptGroup*> vectorOptGroups;
    
    //Find if removing a child will make graph disconnected
    //If true, then fill the vector components with 2 disconnected components
    bool isDisconnected2 (OptGroup* toRemove, std::vector< std::vector <OptGroup*> >& components);

    bool allPrevGroupsAreNotChild (OptGroup* group)
    {        
        for (auto it = group->prevGroups().begin (); it != group->prevGroups().end(); it++)
        {
            if (((*it)->hashID () & hashID()) == (*it)->hashID ())
            {
                return false;
                break;
            }
        }  
        
        return true;
    }
    
    bool allNextGroupsAreNotChild (OptGroup* group)
    {        
        for (auto it = group->nextGroups().begin (); it != group->nextGroups().end(); it++)
        {
            if (((*it)->hashID () & hashID()) == (*it)->hashID ())
            {
                return false;
                break;
            }
        }  
        
        return true;
    }
    
    bool allPrevGroupsAreChild (OptGroup* group)
    {        
        for (auto it = group->prevGroups().begin (); it != group->prevGroups().end(); it++)
        {
            if (((*it)->hashID () & hashID()) != (*it)->hashID ())
            {
                return false;
                break;
            }
        }  
        
        return true;
    }
    
    bool allNextGroupsAreChild (OptGroup* group)
    {        
        for (auto it = group->nextGroups().begin (); it != group->nextGroups().end(); it++)
        {
            if (((*it)->hashID () & hashID()) != (*it)->hashID ())
            {
                return false;
                break;
            }
        }  
        
        return true;
    }
    
    bool isDisconnected (OptGroup* toRemove, vector<unordered_set <OptGroup*> >& components);
    bool createsCycle (OptGroup* toRemove, std::vector< std::vector <OptGroup*> >& components);
    bool minParentEdgeToChild (OptGroup* toRemove, std::vector< unordered_set <OptGroup*> >& components, 
                       OptGroup* minParentGroup);
    void removeChild (uint128_t _hash_id, OptGroup* minParentGroup);
    void setChildrenFromHashID ();
    
    set<OptGroup*, GroupComparer>& childGroups ()
    {
        return children;
    }
    
    inline bool operator==(const OptGroup& other) const
    {
        return other.hash_id == hash_id;
    }
    
    static OptGroup* createOptGroup (uint128_t _hash_id);    
    static bool inParentOptGroupsToHashID (uint128_t _hash_id);    
    static OptGroup* createParentOptGroup (uint128_t _hash_id);    
    static vector<uint128_t>* nextGroupsHashID (uint128_t hash_id);
    static vector<uint128_t>* prevGroupsHashID (uint128_t hash_id);
    static bool isLiveoutForGroup (uint128_t group, OptGroup* child);
    static void liveoutsForGroup (const uint128_t hash_id, 
                                  const vector<Group*>& children, 
                                  unordered_set<Group*, Group::GroupHasher>& liveouts);
    static void liveinsForChildInGroup (uint128_t group, OptGroup* child, 
                                        unordered_set<Group*, Group::GroupHasher>& liveins);
};

#include "Group.impl.hpp"

#endif
